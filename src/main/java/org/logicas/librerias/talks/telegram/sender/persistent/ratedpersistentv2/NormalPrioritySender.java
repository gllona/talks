package org.logicas.librerias.talks.telegram.sender.persistent.ratedpersistentv2;

import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.TalkResponse;

class NormalPrioritySender {

    private static NormalPrioritySender instance = null;

    private final TalksConfiguration talksConfig;
    private final RendevouzQueue rendevouzQueue;

    public static synchronized NormalPrioritySender getInstance(TalksConfiguration config) {
        if (instance == null) {
            instance = new NormalPrioritySender(config);
        }
        return instance;
    }

    public NormalPrioritySender(TalksConfiguration config) {
        this.talksConfig = config;
        rendevouzQueue = RendevouzQueue.getInstance(config);
    }

    public void send(TalkResponse response) {
       rendevouzQueue.acceptNormalPriority(response);
    }

    public void shutdown() {
        // nothing to do
    }
}
