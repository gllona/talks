package org.logicas.librerias.talks.telegram;

import org.logicas.librerias.talks.engine.TalkResponse;

public abstract class MessageSenderApi {

    public abstract void send(TalkResponse response);

    public abstract void shutdown();
}
