package org.logicas.librerias.talks.telegram.sender.notpersistent;

import lombok.AllArgsConstructor;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.TalkResponse;
import org.logicas.librerias.talks.telegram.MessageSenderApi;

@AllArgsConstructor
public class DirectMessageSender extends MessageSenderApi {

    private TalksConfiguration talksConfig;

    @Override
    public void send(TalkResponse response) {
        for (TalkResponse finalResponse : response.getFinalResponses()) {
            talksConfig.getTelegramBot().execute(finalResponse.getTelegramSendRequest());
        }
    }

    @Override
    public void shutdown() {
        talksConfig.getTelegramBot().removeGetUpdatesListener();
    }
}
