package org.logicas.librerias.talks.telegram.sender.notpersistent;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.pengrad.telegrambot.request.AbstractSendRequest;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.config.LibSetting;
import org.logicas.librerias.talks.engine.TalkResponse;
import org.logicas.librerias.talks.telegram.MessageSenderApi;

import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class RatedMessageSender extends MessageSenderApi {

    // Telegram limits:
    // * 4096- chars/msg (DONE at superclass)
    // * 30 msgs/sec to multiple chats (DONE)
    // * 1 msgs/sec same chat (DONE)
    // * 20 msgs/min same group (not planned yet)

    private interface CancellableRunnable extends Runnable {
        void cancel();
    }

    private class Sender implements CancellableRunnable {
        boolean doFinish = false;
        @Override
        public void run() {
            while (! doFinish && ! Thread.currentThread().isInterrupted()) {
                calculateAllChatsDelay();
                sendOneMessage();
            }
            buildFinishSendQueue();
            while (finishSendQueue.size() > 0) {
                sendOneMessageFinishing();
            }
        }
        @Override
        public synchronized void cancel() {
            doFinish = true;
        }
        private void sendOneMessage() {
            Message message = null;
            try {
                message = sendQueue.take();
            } catch (InterruptedException ignored) {
                cancel();
            }
            if (message != null) {
                sendMessage(message);
            }
        }
        private void sendMessage(Message message) {
            message.setSentAt(System.currentTimeMillis());
            try {
                talksConfig.getTelegramBot().execute(message.sendRequest);
                messagesSentInLastSecondCache.put(message, message);
            } catch (Throwable t) {
                logger.error("Exception trying sending message to Telegram", t);
            }
        }
        private void sendOneMessageFinishing() {
            Message message = finishSendQueue.poll();
            if (message != null) {
                sendMessage(message);
            }
        }
    }

    @RequiredArgsConstructor
    @EqualsAndHashCode
    private class Message implements Delayed {
        private final AbstractSendRequest<?> sendRequest;
        private final long sendAt;
        @Getter @Setter private volatile long sentAt = Long.MAX_VALUE;
        @Override
        public long getDelay(TimeUnit timeUnit) {
            long delayInMillis = sendAt - System.currentTimeMillis() + getAllChatsDelay();
            return timeUnit.convert(delayInMillis, TimeUnit.MILLISECONDS);
        }
        @Override
        public int compareTo(Delayed delayed) {
            return (int)(getDelay(TimeUnit.MILLISECONDS) - delayed.getDelay(TimeUnit.MILLISECONDS));
        }
    }

    private static final Logger logger = LogManager.getLogger(RatedMessageSender.class);

    private static final int interMessageDelay = LibSetting.getInteger(LibSetting.CHAT_INTER_MESSAGE_DELAY_IN_MILLIS);
    private static final int maxMessagesPerSecond = LibSetting.getInteger(LibSetting.ALL_CHATS_MAX_MESSAGES_PER_SECOND) - 1;

    private ExecutorService executor;
    private final TalksConfiguration talksConfig;
    private final Map<Thread, Object> threads = Collections.synchronizedMap(new WeakHashMap<>());
    private final DelayQueue<Message> sendQueue = new DelayQueue<>();
    private final Queue<Message> finishSendQueue = new ConcurrentLinkedQueue<>();
    private final Cache<Long, Long> sendAtPerChatCache = CacheBuilder.newBuilder().expireAfterWrite(60, TimeUnit.SECONDS).build();
    private final Cache<Message, Message> messagesSentInLastSecondCache = CacheBuilder.newBuilder().expireAfterWrite(1, TimeUnit.SECONDS).build();
    private long allChatsDelay = 0;
    private volatile boolean finishing = false;

    public RatedMessageSender(TalksConfiguration talksConfig) {
        this.talksConfig = talksConfig;
        createExecutor();
        for (int i = 0; i < LibSetting.getInteger(LibSetting.MESSAGES_SENDER_NUM_THREADS); i++) {
            executor.execute(new Sender());
        }
    }

    private void createExecutor() {
        executor = Executors.newFixedThreadPool(
            LibSetting.getInteger(LibSetting.MESSAGES_SENDER_NUM_THREADS),
            createThreadFactory()
        );
    }

    private ThreadFactory createThreadFactory() {
        return new ThreadFactory() {
            private final AtomicInteger threadCount = new AtomicInteger(0);
            @Override
            public Thread newThread(Runnable r) {
                int id = threadCount.incrementAndGet();
                Thread t = new Thread(r, "Sender thread " + id) {
                    @Override
                    public void interrupt() {
                        logger.info("Interrupting " + getName() + "....");
                        super.interrupt();
                    }
                };
                threads.put(t, "8");
                return t;
            }
        };
    }

    @Override
    public void send(TalkResponse response) {
        for (TalkResponse finalResponse : response.getFinalResponses()) {
            sendQueue.put(
                new Message(finalResponse.getTelegramSendRequest(), addDelayFor(response.getChatId()))
            );
        }
    }

    private long addDelayFor(long chatId) {
        long now = System.currentTimeMillis();
        Long sendAt = sendAtPerChatCache.getIfPresent(chatId);
        if (sendAt == null) {
            sendAt = now;
        } else {
            sendAt = Math.max(sendAt, now - interMessageDelay);
            sendAt += interMessageDelay;
        }
        sendAtPerChatCache.put(chatId, sendAt);
        return sendAt;
    }

    private long getAllChatsDelay() {
        return allChatsDelay;
    }

    private synchronized void setAllChatsDelay(long value) {
        allChatsDelay = value;
    }

    private void calculateAllChatsDelay() {
        long size = messagesSentInLastSecondCache.size();
        if (size < maxMessagesPerSecond) {
            setAllChatsDelay(0);
            return;
        }
        long now = System.currentTimeMillis();
        long timestampOfOldestMessageInLastSecond = messagesSentInLastSecondCache.asMap().keySet().stream()
            .map(Message::getSentAt)
            .min(Long::compareTo)
            .orElse(now);
        setAllChatsDelay(timestampOfOldestMessageInLastSecond - (now - 1000));
    }

    private synchronized void buildFinishSendQueue() {
        if (! finishing) {
            Set<Message> remaining = new TreeSet<>(Comparator.naturalOrder());
            remaining.addAll(sendQueue);
            finishSendQueue.addAll(remaining);
            sendQueue.clear();
            finishing = true;
        }
    }

    @Override
    public void shutdown() {
        logger.info("Stopping message sender...");
        for (Thread thread : threads.keySet()) {
            thread.interrupt();
        }
        logger.info("Shutting down executor...");
        executor.shutdown();
        try {
            executor.awaitTermination(LibSetting.getInteger(LibSetting.SHUTDOWN_TIMEOUT_IN_MILLIS), TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            logger.warn("Interrupted while waiting for RatedMessageSender to finish");
        }
        logger.info("Message sender stopped.");
    }
}
