package org.logicas.librerias.talks.telegram.sender.persistent;

import org.logicas.librerias.copersistance.Dao;
import org.logicas.librerias.copersistance.repositories.SingleKeyEntityRepository;

public class PersistentMessageRepository extends SingleKeyEntityRepository<PersistentMessage> {

    private static PersistentMessageRepository instance;

    private Dao dao;

    private PersistentMessageRepository(Dao dao) {
        this.dao = dao;
    }

    public static synchronized PersistentMessageRepository getInstance(Dao dao) {
        if (instance == null) {
            instance = new PersistentMessageRepository(dao);
        }
        return instance;
    }

    private static final String TABLE = "telegram_message_sender_queue";

    @Override
    protected Dao dao() {
        return dao;
    }

    @Override
    public String tableName() {
        return TABLE;
    }

    @Override
    protected String[] columnNames() {
        return PersistentMessage.FIELDS;
    }

    @Override
    public String idFieldName() {
        return PersistentMessage.FIELD_ID;
    }
}
