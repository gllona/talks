package org.logicas.librerias.talks.telegram.sender.persistent.ratedpersistentv2;

import com.google.common.primitives.Ints;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.logicas.librerias.talks.engine.TalkResponse;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

import static org.logicas.librerias.talks.telegram.sender.persistent.ratedpersistentv2.RatedPersistentMessageSenderV2Facade.DEEP_LOG;

@RequiredArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
class Message implements Delayed {

    private static final Logger logger = LogManager.getLogger(Message.class);

    @Getter private final TalkResponse response;
    @EqualsAndHashCode.Include @Getter private final long dbId;
    private final long sendAt;
    @Getter @Setter private volatile long sentAt = Long.MAX_VALUE;
    private final RendevouzQueue rendevouzQueue;

    @Override
    public long getDelay(TimeUnit timeUnit) {
        long now = System.currentTimeMillis();
        long delayInMillis = sendAt - now + rendevouzQueue.getAllChatsDelay();
        log("GETDELAY FOR DBID=" + dbId + " (" + response.getPriority() + "): SENDAT= " + sendAt + ", DELAY=" + delayInMillis);
        return timeUnit.convert(delayInMillis, TimeUnit.MILLISECONDS);
    }

    @Override
    public int compareTo(Delayed delayed) {
        return Ints.saturatedCast(this.sendAt - ((Message) delayed).sendAt);
    }

    private void log(String s) {
        if (DEEP_LOG) {
            logger.info(s);
        }
    }
}
