package org.logicas.librerias.talks.telegram.sender.persistent.ratedpersistentv2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.config.LibSetting;

import java.io.FileNotFoundException;
import java.util.concurrent.*;

class TelegramSender {

    // Telegram limits:
    // * 4096- chars/msg (DONE at superclass)
    // * 30 msgs/sec to multiple chats (DONE)
    // * 1 msgs/sec same chat (DONE)
    // * 20 msgs/min same group (not planned yet)

    private class Sender implements Runnable {
        private boolean doFinish = false;

        @Override
        public void run() {
            while (! doFinish && ! Thread.currentThread().isInterrupted()) {
                sendOneMessage();
            }
        }

        public synchronized void cancel() {
            doFinish = true;
        }

        private void sendOneMessage() {
            rendevouzQueue.calculateAllChatsDelay();

            if (sendFailedMessage()) {
                return;
            }

            Message message = null;
            try {
                message = rendevouzQueue.getOneMessage();
            } catch (InterruptedException e) {
                cancel();
            }
            if (message != null) {
                sendMessage(message);
            }
        }
    }

    private static final String THREADS_ID = "TSPOOL";
    private static final Logger logger = LogManager.getLogger(TelegramSender.class);

    private static TelegramSender instance;
    private final RendevouzQueue rendevouzQueue;
    private final BlockingQueue<Message> failedMessageQueue = new LinkedBlockingQueue<>();
    private final CustomThreadFactory threadFactory = CustomThreadFactory.getInstance();
    private ExecutorService executor;
    private final TalksConfiguration talksConfig;

    public static synchronized TelegramSender getInstance(TalksConfiguration config) {
        if (instance == null) {
            instance = new TelegramSender(config);
        }
        return instance;
    }

    public TelegramSender(TalksConfiguration talksConfig) {
        this.talksConfig = talksConfig;
        rendevouzQueue = RendevouzQueue.getInstance(talksConfig);
        createExecutor();
        for (int i = 0; i < LibSetting.getInteger(LibSetting.MESSAGES_SENDER_NUM_THREADS); i++) {
            executor.execute(new Sender());
        }
    }

    private void createExecutor() {
        executor = Executors.newFixedThreadPool(
            LibSetting.getInteger(LibSetting.MESSAGES_SENDER_NUM_THREADS),
            threadFactory.create(THREADS_ID)
        );
    }

    private void sendMessage(Message message) {
        long now = System.currentTimeMillis();
        boolean sent = false;

        try {
            talksConfig.getTelegramBot().execute(message.getResponse().getTelegramSendRequest());
            sent = true;
        } catch (Exception e) {
            if (e.getCause() instanceof FileNotFoundException) {
                sent = true; // do not retry the send operation
                logger.error("Exception trying sending message to Telegram because file was not found.", e);
            } else {
                enqueueFailedMessage(message);
                logger.error("Exception trying sending message to Telegram, will retry...", e);
            }
        }

        if (sent) {
            rendevouzQueue.markMessageAsSent(message, now);
        }
    }

    private void enqueueFailedMessage(Message message) {
        try {
            failedMessageQueue.put(message);
        } catch (InterruptedException ignored) {
        }
    }

    private boolean sendFailedMessage() {
        Message message = failedMessageQueue.poll();
        if (message == null) {
            return false;
        }

        sendMessage(message);

        return true;
    }

    public void shutdown() {
        logger.info("Stopping Telegram sender...");
        threadFactory.interruptThreads(THREADS_ID);
        logger.info("Shutting down executor...");
        executor.shutdown();
        try {
            executor.awaitTermination(LibSetting.getInteger(LibSetting.SHUTDOWN_TIMEOUT_IN_MILLIS), TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            logger.warn("Interrupted while waiting for TelegramSender to finish");
        }
        logger.info("Telegram sender stopped.");
    }
}
