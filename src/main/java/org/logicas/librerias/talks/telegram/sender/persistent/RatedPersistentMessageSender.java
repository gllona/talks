package org.logicas.librerias.talks.telegram.sender.persistent;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.config.LibSetting;
import org.logicas.librerias.talks.engine.TalkResponse;
import org.logicas.librerias.talks.telegram.MessageSenderApi;

import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static org.logicas.librerias.talks.engine.SendPriority.LOW;

public class RatedPersistentMessageSender extends MessageSenderApi {

    // Telegram limits:
    // * 4096- chars/msg (DONE at superclass)
    // * 30 msgs/sec to multiple chats (DONE)
    // * 1 msgs/sec same chat (DONE)
    // * 20 msgs/min same group (not planned yet)

    private static final boolean DEEP_LOG = false;

    private interface CancellableRunnable extends Runnable {
        void cancel();
    }

    private class Sender implements CancellableRunnable {
        boolean doFinish = false;
        @Override
        public void run() {
            while (! doFinish && ! Thread.currentThread().isInterrupted()) {
                calculateAllChatsDelay();
                sendOneMessage();
            }
        }
        @Override
        public synchronized void cancel() {
            doFinish = true;
        }
        private void sendOneMessage() {
            Message message = null;
            try {
                message = sendQueue.take();
            } catch (InterruptedException ignored) {
                cancel();
            }
            if (message != null) {
                sendMessage(message);
            }
        }
        private void sendMessage(Message message) {
            long now = System.currentTimeMillis();
            message.setSentAt(now);
            try {
                talksConfig.getTelegramBot().execute(message.getResponse().getTelegramSendRequest());
            } catch (Throwable t) {
                logger.error("Exception trying sending message to Telegram", t);
            } finally {
                messagesSentInLastSecondCache.put(message, message);
                if (message.getResponse().getPriority() == LOW) {
                    sendAtPerChatCache.put(message.getResponse().getChatId(), now);
                    synchronized (lastSentLowPrioritySequenceMonitor) {
                        if (message.getLowPrioritySequence() > lastSentLowPrioritySequence.get()) {
                            lastSentLowPrioritySequence.set(message.getLowPrioritySequence());
                            log("SETTING LOWPRIORITYMESSAGESEQUENCE TO " + message.getLowPrioritySequence());
                        }
                    }
                }
                PersistentMessage persistentMessage = messageRepository.readById(message.getDbId());
                if (persistentMessage != null) {
                    messageRepository.delete(persistentMessage);
                }
            }
        }
    }

    @RequiredArgsConstructor
    @EqualsAndHashCode
    private class Message implements Delayed {
        @Getter private final TalkResponse response;
        @Getter private final long dbId;
        private final long sendAt;
        @Getter private final int lowPrioritySequence;
        @Getter @Setter private volatile long sentAt = Long.MAX_VALUE;
        @Override
        public long getDelay(TimeUnit timeUnit) {
            long now = System.currentTimeMillis();
            long sendingTime = getSendingTime(now);
            long delayInMillis = sendingTime - now + getAllChatsDelay();
            lastMessageInGetDelayHolder.setMessage(this);
            log("GETDELAY FOR DBID=" + dbId + " (" + response.getPriority() + "): SENDINGTIME= " + sendingTime + ", DELAY=" + delayInMillis);
            return timeUnit.convert(delayInMillis, TimeUnit.MILLISECONDS);
        }
        @Override
        public int compareTo(Delayed delayed) {
            return (int)(getDelay(TimeUnit.MILLISECONDS) - delayed.getDelay(TimeUnit.MILLISECONDS));
        }
        private long getSendingTime(long now) {
            switch (response.getPriority()) {
                case NORMAL:
                    return sendAt;
                case LOW:
                    Long baseTime = sendAtPerChatCache.getIfPresent(response.getChatId());
                    if (baseTime == null) {
                        baseTime = now - interMessageDelay;
                    }
                    baseTime += interMessageDelay;
                    int additionalDelayInMessages = lowPrioritySequence - lastSentLowPrioritySequence.get() - 1;
                    int additionalDelayInMillis = additionalDelayInMessages * interMessageDelay;
                    additionalDelayInMillis = fixDelay(additionalDelayInMillis);
                    long sendingTimeInMillis = baseTime + additionalDelayInMillis;
                    log("GETSENDINGTIME FOR DBID=" + dbId + " ADDITIONALDELAYINMESSAGES=" + additionalDelayInMessages);
                    return sendingTimeInMillis;
                default:
                    throw new RuntimeException("Unsupported message priority " + response.getPriority());
            }
        }
        private int fixDelay(int delayInMillis) {
            if (lastMessageInGetDelayHolder.getMessage() != this) {
                return delayInMillis;
            } else if (lastMessageInGetDelayHolder.getGetDelayCalledTimes() < 2) {
                return delayInMillis;
            } else {
                log("EXECUTED FIXDELAY FOR DBID=" + dbId);
                return 0;
            }
        }
    }

    private class MessageHolder {
        private Message message = null;
        private int getDelayCalledTimes = 0;
        public synchronized Message getMessage() {
            return message;
        }
        public synchronized void setMessage(Message message) {
            if (this.message != message) {
                this.message = message;
                getDelayCalledTimes = 0;
            } else {
                getDelayCalledTimes++;
            }
        }
        public synchronized int getGetDelayCalledTimes() {
            return getDelayCalledTimes;
        }
    }

    private static final Logger logger = LogManager.getLogger(RatedPersistentMessageSender.class);

    private static final int interMessageDelay = LibSetting.getInteger(LibSetting.CHAT_INTER_MESSAGE_DELAY_IN_MILLIS);
    private static final int maxMessagesPerSecond = LibSetting.getInteger(LibSetting.ALL_CHATS_MAX_MESSAGES_PER_SECOND) - 1;

    private ExecutorService executor;
    private final TalksConfiguration talksConfig;
    private final Map<Thread, Object> threads = Collections.synchronizedMap(new WeakHashMap<>());
    private final PersistentMessageRepository messageRepository;
    private final DelayQueue<Message> sendQueue = new DelayQueue<>();
    private final Cache<Long, Long> sendAtPerChatCache = CacheBuilder.newBuilder().expireAfterWrite(60, TimeUnit.SECONDS).build();
    private final Cache<Message, Message> messagesSentInLastSecondCache = CacheBuilder.newBuilder().expireAfterWrite(1, TimeUnit.SECONDS).build();
    private final MessageHolder lastMessageInGetDelayHolder = new MessageHolder();
    private final AtomicInteger lastQueuedLowPrioritySequence = new AtomicInteger(-1);
    private final AtomicInteger lastSentLowPrioritySequence = new AtomicInteger(-1);
    private final Object lastSentLowPrioritySequenceMonitor = new Object();
    private long allChatsDelay = 0;

    public RatedPersistentMessageSender(TalksConfiguration talksConfig) {
        this.talksConfig = talksConfig;
        createExecutor();
        messageRepository = PersistentMessageRepository.getInstance(talksConfig.getDao());
        queuePendingMessages();
        for (int i = 0; i < LibSetting.getInteger(LibSetting.MESSAGES_SENDER_NUM_THREADS); i++) {
            executor.execute(new Sender());
        }
    }

    private void createExecutor() {
        executor = Executors.newFixedThreadPool(
            LibSetting.getInteger(LibSetting.MESSAGES_SENDER_NUM_THREADS),
            createThreadFactory()
        );
    }

    private ThreadFactory createThreadFactory() {
        return new ThreadFactory() {
            private final AtomicInteger threadCount = new AtomicInteger(0);
            @Override
            public Thread newThread(Runnable r) {
                int id = threadCount.incrementAndGet();
                Thread t = new Thread(r, "Sender thread " + id) {
                    @Override
                    public void interrupt() {
                        logger.info("Interrupting " + getName() + "....");
                        super.interrupt();
                    }
                };
                threads.put(t, "14");
                return t;
            }
        };
    }

    @Override
    public void send(TalkResponse response) {
        for (TalkResponse finalResponse : response.getFinalResponses()) {
            PersistentMessage persistentMessage = new PersistentMessage();
            persistentMessage.setUnencodedTalkResponse(finalResponse);
            messageRepository.save(persistentMessage);
            Message message = buildMessage(finalResponse, persistentMessage.getId());
            sendQueue.put(message);
        }
    }

    private Message buildMessage(TalkResponse response, Long persistentMessageId) {
        return new Message(
            response,
            persistentMessageId,
            addDelayFor(response),
            response.getPriority() == LOW ? lastQueuedLowPrioritySequence.incrementAndGet() : 0
        );
    }

    private long addDelayFor(TalkResponse response) {
        switch (response.getPriority()) {
            case LOW:
                return 0;   // ignored by the sender
            case NORMAL:
                long now = System.currentTimeMillis();
                Long sendAt = sendAtPerChatCache.getIfPresent(response.getChatId());
                if (sendAt == null) {
                    sendAt = now;
                } else {
                    sendAt = Math.max(sendAt, now - interMessageDelay);
                    sendAt += interMessageDelay;
                }
                sendAtPerChatCache.put(response.getChatId(), sendAt);
                return sendAt;
            default:
                throw new RuntimeException("Unsupported message priority " + response.getPriority());
        }
    }

    private long getAllChatsDelay() {
        return allChatsDelay;
    }

    private synchronized void setAllChatsDelay(long value) {
        allChatsDelay = value;
    }

    private void calculateAllChatsDelay() {
        long delay;
        if (messagesSentInLastSecondCache.size() < maxMessagesPerSecond) {
            delay = 0L;
        } else {
            long now = System.currentTimeMillis();
            long timestampOfOldestMessageInLastSecond = messagesSentInLastSecondCache.asMap().keySet().stream()
                .map(Message::getSentAt)
                .min(Long::compareTo)
                .orElse(now);
            delay = timestampOfOldestMessageInLastSecond - (now - 1000);
        }
        setAllChatsDelay(delay);
        log("SET ALL CHATS DELAY TO " + delay);
    }

    private void queuePendingMessages() {
        List<PersistentMessage> pendingPersistentMessages = readSortedPendingMessages();
        for (PersistentMessage persistentMessage : pendingPersistentMessages) {
            TalkResponse response = persistentMessage.getUnencodedTalkResponse();
            Message message = buildMessage(response, persistentMessage.getId());
            sendQueue.put(message);
            log("QUEUED MESSAGE DBID=" + persistentMessage.getId() + " (" + response.getPriority() + ")");
        }
    }

    private List<PersistentMessage> readSortedPendingMessages() {
        return messageRepository.readAll().stream()
            .sorted(Comparator.comparing(PersistentMessage::getId))
            .collect(Collectors.toList());
    }

    @Override
    public void shutdown() {
        logger.info("Stopping message sender...");
        for (Thread thread : threads.keySet()) {
            thread.interrupt();
        }
        logger.info("Shutting down executor...");
        executor.shutdown();
        try {
            executor.awaitTermination(LibSetting.getInteger(LibSetting.SHUTDOWN_TIMEOUT_IN_MILLIS), TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            logger.warn("Interrupted while waiting for RatedPersistentMessageSender to finish");
        }
        logger.info("Message sender stopped.");
    }

    private void log(String s) {
        if (DEEP_LOG) {
            logger.info(s);
        }
    }
}
