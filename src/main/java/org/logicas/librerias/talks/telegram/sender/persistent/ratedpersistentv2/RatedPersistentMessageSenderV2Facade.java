package org.logicas.librerias.talks.telegram.sender.persistent.ratedpersistentv2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.SendPriority;
import org.logicas.librerias.talks.engine.TalkResponse;
import org.logicas.librerias.talks.telegram.MessageSenderApi;

import static org.logicas.librerias.talks.engine.SendPriority.LOW;
import static org.logicas.librerias.talks.engine.SendPriority.NORMAL;

public class RatedPersistentMessageSenderV2Facade extends MessageSenderApi {

    // Telegram limits:
    // * 4096- chars/msg (DONE at superclass)
    // * 30 msgs/sec to multiple chats (DONE)
    // * 1 msgs/sec same chat (DONE)
    // * 20 msgs/min same group (not planned yet)

    static final boolean DEEP_LOG = false;

    private static final Logger logger = LogManager.getLogger(RatedPersistentMessageSenderV2Facade.class);

    private final TalksConfiguration talksConfig;
    private final NormalPrioritySender normalPrioritySender;
    private final LowPrioritySender lowPrioritySender;
    private final RendevouzQueue rendevouzQueue;
    private final TelegramSender telegramSender;

    public RatedPersistentMessageSenderV2Facade(TalksConfiguration talksConfig) {
        this.talksConfig = talksConfig;
        this.rendevouzQueue = RendevouzQueue.getInstance(talksConfig);
        this.telegramSender = TelegramSender.getInstance(talksConfig);
        this.normalPrioritySender = NormalPrioritySender.getInstance(talksConfig);
        this.lowPrioritySender = LowPrioritySender.getInstance(talksConfig);

        logger.info("RatedPersistentMessageSenderV2 started.");
    }

    @Override
    public void send(TalkResponse response) {
        SendPriority priority = response.getPriority();

        if (priority == NORMAL) {
            normalPrioritySender.send(response);
        } else if (priority == LOW) {
            lowPrioritySender.send(response);
        } else {
            throw new RuntimeException("Unsupported TalkResponse priority " + priority);
        }
    }

    @Override
    public void shutdown() {
        logger.info("Stopping rated persistent message sender V2...");
        normalPrioritySender.shutdown();
        lowPrioritySender.shutdown();
        telegramSender.shutdown();
        rendevouzQueue.shutdown();
        logger.info("Message sender stopped.");
    }
}
