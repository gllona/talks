package org.logicas.librerias.talks.telegram.sender.persistent.ratedpersistentv2;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.config.LibSetting;
import org.logicas.librerias.talks.engine.TalkResponse;
import org.logicas.librerias.talks.telegram.sender.persistent.PersistentMessage;
import org.logicas.librerias.talks.telegram.sender.persistent.PersistentMessageRepository;

import java.util.Comparator;
import java.util.List;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static org.logicas.librerias.talks.telegram.sender.persistent.ratedpersistentv2.RatedPersistentMessageSenderV2Facade.DEEP_LOG;

class RendevouzQueue {

    private static RendevouzQueue instance = null;
    private static final Logger logger = LogManager.getLogger(RendevouzQueue.class);

    private final TalksConfiguration talksConfig;
    private final PersistentMessageRepository messageRepository;
    private final DelayQueue<Message> sendQueue = new DelayQueue<>();
    private final Cache<Message, Message> messagesSentInLastSecondCache = CacheBuilder.newBuilder().expireAfterWrite(1, TimeUnit.SECONDS).build();
    private final Cache<Long, Long> sendAtPerChatCache = CacheBuilder.newBuilder().expireAfterWrite(60, TimeUnit.SECONDS).build();
    private final Object acceptMonitor = new Object();
    private volatile long allChatsDelay = 0;

    public static synchronized RendevouzQueue getInstance(TalksConfiguration config) {
        if (instance == null) {
            instance = new RendevouzQueue(config);
        }
        return instance;
    }

    public RendevouzQueue(TalksConfiguration config) {
        this.talksConfig = config;
        messageRepository = PersistentMessageRepository.getInstance(config.getDao());
        queuePendingMessages();
    }

    private void queuePendingMessages() {
        List<PersistentMessage> pendingPersistentMessages = readSortedPendingMessages();
        for (PersistentMessage persistentMessage : pendingPersistentMessages) {
            TalkResponse response = persistentMessage.getUnencodedTalkResponse();
            Message message = buildMessage(response, persistentMessage.getId());
            sendQueue.put(message);
            log("QUEUED MESSAGE DBID=" + persistentMessage.getId() + " (" + response.getPriority() + ")");
        }
    }

    private List<PersistentMessage> readSortedPendingMessages() {
        return messageRepository.readAll().stream()
            .sorted(Comparator.comparing(PersistentMessage::getId))
            .collect(Collectors.toList());
    }

    public void acceptNormalPriority(TalkResponse response) {
        synchronized (acceptMonitor) {
            accept(response);
        }
    }

    public boolean acceptLowPriority(TalkResponse response) {
        synchronized (acceptMonitor) {
            if (sendQueue.size() == 0) {
                accept(response);
                return true;
            } else {
                return false;
            }
        }
    }

    private void accept(TalkResponse response) {
        for (TalkResponse finalResponse : response.getFinalResponses()) {
            PersistentMessage persistentMessage = new PersistentMessage();
            persistentMessage.setUnencodedTalkResponse(finalResponse);
            messageRepository.save(persistentMessage);
            Message message = buildMessage(finalResponse, persistentMessage.getId());
            sendQueue.put(message);
        }
    }

    private Message buildMessage(TalkResponse response, Long persistentMessageId) {
        return new Message(
            response,
            persistentMessageId,
            addDelayFor(response),
            this
        );
    }

    private long addDelayFor(TalkResponse response) {
        switch (response.getPriority()) {
            case LOW:
                return 0;   // ignored by the sender
            case NORMAL:
                long now = System.currentTimeMillis();
                Long sendAt = sendAtPerChatCache.getIfPresent(response.getChatId());
                if (sendAt == null) {
                    sendAt = now;
                } else {
                    sendAt = Math.max(sendAt, now - getInterMessageDelay());
                    sendAt += getInterMessageDelay();
                }
                sendAtPerChatCache.put(response.getChatId(), sendAt);
                return sendAt;
            default:
                throw new RuntimeException("Unsupported message priority " + response.getPriority());
        }
    }

    Message getOneMessage() throws InterruptedException {
        return sendQueue.take();
    }

    void markMessageAsSent(Message message, long sendTimestamp) {
        message.setSentAt(sendTimestamp);
        messagesSentInLastSecondCache.put(message, message);
        PersistentMessage persistentMessage = messageRepository.readById(message.getDbId());
        if (persistentMessage != null) {
            messageRepository.delete(persistentMessage);
        }
    }

    void calculateAllChatsDelay() {
        long delay;
        if (messagesSentInLastSecondCache.size() < getAllChatsMaxMessagesPerSecond()) {
            delay = 0L;
        } else {
            long now = System.currentTimeMillis();
            long timestampOfOldestMessageInLastSecond = messagesSentInLastSecondCache.asMap().keySet().stream()
                .map(Message::getSentAt)
                .min(Long::compareTo)
                .orElse(now);
            delay = timestampOfOldestMessageInLastSecond - (now - 1000);
        }
        setAllChatsDelay(delay);
        log("SET ALL CHATS DELAY TO " + delay);
    }

    long getAllChatsDelay() {
        return allChatsDelay;
    }

    private synchronized void setAllChatsDelay(long value) {
        allChatsDelay = value;
    }

    public void shutdown() {
        // nothing to do
    }

    private void log(String s) {
        if (DEEP_LOG) {
            logger.info(s);
        }
    }

    private static int getInterMessageDelay() {
        return LibSetting.getInteger(LibSetting.CHAT_INTER_MESSAGE_DELAY_IN_MILLIS);
    }

    private static int getAllChatsMaxMessagesPerSecond() {
        return LibSetting.getInteger(LibSetting.ALL_CHATS_MAX_MESSAGES_PER_SECOND) - 1;
    }
}
