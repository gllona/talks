package org.logicas.librerias.talks.engine;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.api.TracerApi;
import org.logicas.librerias.talks.config.LibSetting;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;

@EqualsAndHashCode
public abstract class Talk implements Comparable<Talk> {

    protected static Logger logger = LogManager.getLogger(Talk.class);

    private static List<InputHandler> PRESERVE_HANDLERS = singletonList(new InputHandler(emptyList(), "", ""));
    private static List<String> talksClassNames = null;

    protected TalksConfiguration talksConfig;
    protected TracerApi tracer;
    protected TalkType type;
    @Getter @Setter protected TalkContext context;

    protected Talk(TalksConfiguration talksConfig, TalkType talkType) {
        this.talksConfig = talksConfig;
        this.type = talkType;
        tracer = talksConfig.getTracer();
        if (talksClassNames == null) {
            talksClassNames = talksConfig.getTalkTypes().stream()
                .map(TalkType::getTalkClass)
                .map(Class::getSimpleName)
                .collect(Collectors.toList());
        }
    }

    protected abstract Optional<InputHandler> getDefaultHandler();

    public TalkType getType() {
        return type;
    }

    protected synchronized void createContext() {
        if (context == null) {
            context = TalkContext.create(talksConfig, this);
        }
    }

    protected synchronized void createContext(String useContextFromThisClassName) {
        if (context == null) {
            context = TalkContext.create(useContextFromThisClassName, this);
        }
    }

    protected synchronized void createContext(Class<? extends Talk> useContextFromThisClass) {
        if (context == null) {
            context = TalkContext.create(useContextFromThisClass, this);
        }
    }

    public Optional<TalkResponse> dispatch(TalkRequest call) {
        if (call.getMessage() == null) {
            return dispatchWithNullMessage(call);
        }

        Optional<InputMatch> match = context.match(call);
        if (match.isPresent()) {
            long startedAt = System.currentTimeMillis();
            String className = match.get().getHandler().getClassName();
            String methodName = match.get().getHandler().getMethodName();
            tracer.info(call.getChatId(), String.format("To dispatch TalkRequest to method %s::%s....", className, methodName));
            TalkResponse response = invokeMethod(match.get(), call);
            if (response == null) {
                tracer.warn(call.getChatId(), String.format("A [null] response was received from method %s::%s....", className, methodName));
            } else {
                long endedAt = System.currentTimeMillis();
                response.setTelemetry(new TalkResponse.Telemetry(startedAt, endedAt, className, methodName));
            }
            tracer.info(call.getChatId(), String.format("Finished invocation of method %s::%s....", className, methodName));
            return Optional.ofNullable(response);
        }

        //TODO process message :: new_chat_members=null, left_chat_member=null, new_chat_title='null', new_chat_photo=null, delete_chat_photo=null, group_chat_created=null, supergroup_chat_created=null, channel_chat_created=null, migrate_to_chat_id=null, migrate_from_chat_id=null, pinned_message=null, invoice=null, successful_payment=null, connected_website='null', passport_data=null, reply_markup=null

        return Optional.empty();
    }

    public void saveHandlers(long chatId, TalkResponse response) {
        if (response.getHandlers() == null) {
            tracer.warn(chatId, "A response with [null] handlers was received.");
        } else if (! response.getHandlers().equals(Talk.preserveHandlers())) {
            context.deleteBoundedHandlers(chatId);
            context.setHandlers(chatId, response.getHandlers());
        }
    }

    private Optional<TalkResponse> dispatchWithNullMessage(TalkRequest call) {
        //TODO process inline query and :: channel_post=null, edited_channel_post=null, inline_query=null, chosen_inline_result=null, callback_query=null, shipping_query=null, pre_checkout_query=null, poll=null, poll_answer=null
        logger.info(String.format("Received an update [%d] with null message: ", call.getUpdate().updateId()));
        return Optional.empty();
    }

    private TalkResponse invokeMethod(InputMatch match, TalkRequest call) {
        call.setMatch(match);
        try {
            String className = match.getHandler().getClassName();
            className = getTalkPackageName() + "." + className;
            Class<?> clazz = Class.forName(className);
            Method m = clazz.getDeclaredMethod(match.getHandler().getMethodName(), TalkRequest.class);
            Talk target = talksConfig.getTalkManager().talkForName(clazz.getSimpleName());
            TalkResponse response = (TalkResponse) m.invoke(target, call);
            return response;
        } catch (Exception e) {
            throw new RuntimeException(String.format(
                "Unable to invoke method [%s::%s] successfully", match.getHandler().getClassName(), match.getHandler().getMethodName()
            ), e);
        }
    }

    public static List<InputHandler> preserveHandlers() {
        return PRESERVE_HANDLERS;
    }

    @SuppressWarnings("unchecked")
    protected <T extends Talk> T talkForClass(Class<T> clazz) {
        return (T) talksConfig.getTalkManager().talkForName(clazz.getSimpleName());
    }

    private String getTalkPackageName() {
        return LibSetting.get(LibSetting.TALK_PACKAGE_NAME);
    }

    @Override
    public int compareTo(Talk other) {   //TODO improve
        String thisClassName = this.getClass().getSimpleName();
        String otherClassName = other.getClass().getSimpleName();
        int thisClassIndex = talksClassNames.indexOf(thisClassName);
        int otherClassIndex = talksClassNames.indexOf(otherClassName);
        return thisClassIndex - otherClassIndex;
    }
}
