package org.logicas.librerias.talks.engine;

import org.logicas.librerias.copersistance.Dao;

public class PersonalDataRemover {

    public static PersonalDataRemover instance = null;

    PersistentTalkContextRepository talkContextRepository;

    public static synchronized PersonalDataRemover getInstance(Dao dao) {
        if (instance == null) {
            instance = new PersonalDataRemover(dao);
        }
        return instance;
    }

    public PersonalDataRemover(Dao dao) {
        talkContextRepository = PersistentTalkContextRepository.getInstance(dao);
    }

    public void remove(long session) {
        talkContextRepository.deleteVars(session);
        talkContextRepository.deleteHandlers(session);
    }
}
