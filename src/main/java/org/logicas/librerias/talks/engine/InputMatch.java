package org.logicas.librerias.talks.engine;

import com.pengrad.telegrambot.model.Message;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import org.logicas.librerias.talks.engine.InputTrigger.GroupAction;

@Getter
public class InputMatch {

    @Setter private InputHandler handler;
    private final Message message;
    private final MediaType mediaType;
    private final Object mediaMatch;
    @Setter private List<String> regexMatches;
    private final GroupAction groupAction;

    public InputMatch(Message message, MediaType mediaType, Object mediaMatch, List<String> regexMatches, GroupAction groupAction) {
        this.handler = null;
        this.message = message;
        this.mediaType = mediaType;
        this.mediaMatch = mediaMatch;
        this.regexMatches = regexMatches;
        this.groupAction = groupAction;
    }
}
