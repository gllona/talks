package org.logicas.librerias.talks.engine;

public enum SendPriority {
    NORMAL,
    LOW
}
