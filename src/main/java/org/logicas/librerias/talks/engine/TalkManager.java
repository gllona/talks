package org.logicas.librerias.talks.engine;

import com.pengrad.telegrambot.model.Message;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.logicas.librerias.talks.Coroutines;
import org.logicas.librerias.talks.api.TalkManagerApi;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.config.LibSetting;

import java.util.*;
import java.util.stream.Collectors;

public class TalkManager implements TalkManagerApi {

    private static Logger logger = LogManager.getLogger(TalkManager.class);

    private static TalkManagerApi instance;

    private TalksConfiguration talksConfig;
    private Map<Talk, Optional<InputHandler>> talks = new TreeMap<>();

    public static synchronized TalkManagerApi getInstance(TalksConfiguration talksConfig) {
        if (instance == null) {
            instance = new TalkManager(talksConfig);
        }
        return instance;
    }

    private TalkManager(TalksConfiguration talksConfig) {
        logger.info("Starting TalkManager....");
        this.talksConfig = talksConfig;
        createTalks(talksConfig.getTalkTypes());
        setupGlobalHandlers();
        logger.info("TalkManager started.");
    }

    private void createTalks(List<TalkType> talkTypes) {
        for (TalkType talkType : talkTypes) {
            Talk talk = instanceTalk(talkType);
            Optional<InputHandler> handler = talk.getDefaultHandler();
            talks.put(talk, handler);
        }
    }

    private Talk instanceTalk(TalkType talkType) {
        try {
            return talkType.getTalkClass()
                .getDeclaredConstructor(TalksConfiguration.class, TalkType.class)
                .newInstance(talksConfig, talkType);
        } catch (Exception e) {
            throw new RuntimeException(String.format("Can not instance class [%s]", talkType.getTalkClass().getSimpleName()), e);
        }
    }

    private void setupGlobalHandlers() {
        List<InputHandler> allHandlers = new ArrayList<>(talks.values()).stream()
            .filter(Optional::isPresent)
            .map(Optional::get)
            .collect(Collectors.toList());
        for (Talk entry : talks.keySet()) {
            entry.getContext().setGlobalHandlers(allHandlers);
        }
    }

    @Override
    public void resetTalkContexts(long session) {
        for (Talk talk : getTalks()) {
            talk.getContext().reset(session);
        }
    }

    @Override
    public void flushTalkContexts(long session) {
        for (Talk talk : getTalks()) {
            talk.getContext().flush(session);
        }
    }

    @Override
    public Optional<TalkResponse> dispatch(TalkRequest call) {
        updateCallFields(call);

        for (Talk talk : getTalks()) {
            Optional<TalkResponse> response = talk.dispatch(call);

            talksConfig.getInteractionsManager().handle(call);

            if (response.isPresent()) {
                talk.saveHandlers(call.getChatId(), response.get().getLastResponseInChain());
                return response;
            }
        }

        return buildFallbackResponse(call);
    }

    private Optional<TalkResponse> buildFallbackResponse(TalkRequest call) {
        String fallbackResponse = getFallbackResponse().replace("\\n", "\n");

        if (fallbackResponse.length() == 0) {
            logger.info("Returning an empty TalkResponse for Update [{}]", call.getUpdate().updateId());
            return Optional.empty();
        }

        return Optional.of(
            TalkResponse.ofText(
                call.getChatId(),
                fallbackResponse
            ).withPreserveHandlers()
        );
    }

    private void updateCallFields(TalkRequest call) {
        if (isJoinGroupMessage(call.getMessage())) {
            call.setJoinGroup(true);
        }
        if (isLeaveGroupMessage(call.getMessage())) {
            call.setLeaveGroup(true);
        }
    }

    private boolean isJoinGroupMessage(Message message) {
        if (message == null || message.newChatMembers() == null) {
            return false;
        }
        String botName = getBotName();
        return Arrays.stream(message.newChatMembers())
                     .anyMatch(user -> botName.equalsIgnoreCase(user.username()));
    }

    private boolean isLeaveGroupMessage(Message message) {
        if (message == null || message.leftChatMember() == null) {
            return false;
        }
        String botName = getBotName();
        return botName.equalsIgnoreCase(message.leftChatMember().username());
    }

    private List<Talk> getTalks() {
        return new ArrayList<>(talks.keySet());
    }

    // invocation assumes Talks enum elements are same name as their classes //TODO
    @Override
    public Talk talkForName(String simpleClassName) {
        for (Talk talk : talks.keySet()) {
            if (talk.getClass().getSimpleName().equals(simpleClassName)) {
                return talk;
            }
        }
        return null;
    }

    @Override
    public void shutDown() {
        logger.info("Stopping TalkManager....");
        Coroutines.cancelRemaining();
        talksConfig.getDao().shutdown();
        logger.info("TalkManager stopped.");
    }

    private String getBotName() {
        return LibSetting.get(LibSetting.BOT_NAME);
    }

    private String getFallbackResponse() {
        return LibSetting.get(LibSetting.FALLBACK_RESPONSE);
    }
}
