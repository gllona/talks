package org.logicas.librerias.talks.engine;

import org.logicas.librerias.copersistance.Dao;
import org.logicas.librerias.copersistance.repositories.SingleKeyEntityRepository;

import java.util.List;

public class InteractionRepository extends SingleKeyEntityRepository<Interaction> {

    private static InteractionRepository instance;

    private Dao dao;

    private InteractionRepository(Dao dao) {
        this.dao = dao;
    }

    public static synchronized InteractionRepository getInstance(Dao dao) {
        if (instance == null) {
            instance = new InteractionRepository(dao);
        }
        return instance;
    }

    private static final String TABLE = "talks_interactions";

    @Override
    protected Dao dao() {
        return dao;
    }

    @Override
    public String tableName() {
        return TABLE;
    }

    @Override
    protected String[] columnNames() {
        return Interaction.FIELDS;
    }

    @Override
    public String idFieldName() {
        return Interaction.FIELD_ID;
    }

    public List<Interaction> readToFly() {
        return readByCondition(Interaction.FIELD_FLY_AT + " <= NOW()");
    }

    public List<Interaction> readToFlyStealth(int stealthFactor) {
        return readByCondition(
            "NOW() - TIMESTAMPADD(MICROSECOND, - TIMESTAMPDIFF(" +
                "MICROSECOND, " +
                Interaction.FIELD_FLY_AT + ", " +
                Interaction.FIELD_CREATED_AT + ") " +
            "/ " + stealthFactor + ", " + Interaction.FIELD_CREATED_AT + ") >= 0");
    }
}
