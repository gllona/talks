package org.logicas.librerias.talks.engine;

import com.pengrad.telegrambot.model.request.ParseMode;
import com.pengrad.telegrambot.request.*;
import lombok.*;
import org.logicas.librerias.talks.config.LibSetting;
import org.logicas.librerias.talks.telegram.ButtonDecorator;

import java.io.File;
import java.net.URI;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.logicas.librerias.talks.engine.SendPriority.LOW;
import static org.logicas.librerias.talks.engine.SendPriority.NORMAL;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Getter
public class TalkResponse {

    @EqualsAndHashCode
    static class FileBasedPayload {
        public URI fileURI;
        public FileBasedPayload(File file) {
            this.fileURI = file.toURI();
        }
        public File file() {
            return Paths.get(fileURI).toFile();
        }
    }

    @AllArgsConstructor
    @EqualsAndHashCode
    static class ContactPayload {
        public String phoneNumber;
        public String firstName;
    }

    @AllArgsConstructor
    @EqualsAndHashCode
    static class LocationPayload {
        public float latitude;
        public float longitude;
    }

    @AllArgsConstructor
    @EqualsAndHashCode
    @Getter
    public static class Telemetry {
        private long startedAtInMillis;
        private long endedAtInMillis;
        private String className;
        private String methodName;
        public long getDurationInMillis() {
            return endedAtInMillis - startedAtInMillis;
        }
        public String getClassAndMethod() {
            return className + "::" + methodName;
        }
    }

    private static final int MAX_TEXT_MESSAGE_LENGTH = 4000;   // 4096 by Telegram specs

    private MediaType mediaType;

    private String text;
    private boolean isHtml;
    private FileBasedPayload fileBasedPayload;
    @Getter(value = AccessLevel.NONE) private ContactPayload contactPayload;
    @Getter(value = AccessLevel.NONE) private LocationPayload locationPayload;
    private long chatId;
    private Long messageId; // used for actions on messages
    private List<InputHandler> handlers;
    private SendPriority priority = NORMAL;
    private Long flyTime;
    private transient AbstractSendRequest<?> telegramSendRequest;
    private final transient List<Consumer<AbstractSendRequest<?>>> decorators = new ArrayList<>();

    private transient Supplier<String> textSupplier;
    private transient Supplier<File> fileSupplier;
    private transient Function<Exception, TalkResponse> exceptionMapper;

    private TalkResponse nextResponse;

    @Setter private transient Telemetry telemetry;

    public long getChatId() {
        return chatId;
    }

    public Long getFlyTime() {
        return flyTime;
    }

    public SendPriority getPriority() {
        return priority;
    }

    public Function<Exception, TalkResponse> getExceptionMapper() {
        return exceptionMapper;
    }

    public TalkResponse getNextResponse() {
        return nextResponse;
    }

    public MediaType getMediaType() {
        return mediaType;
    }

    public String getText() {
        if (text == null && textSupplier != null) {
            try {
                text = textSupplier.get();
            } catch (Exception e) { // receiver will get null and react to it
            }
        }
        return text;
    }

    public String getFastText() {
        return text == null && textSupplier != null ? "[text-not-yet-calculated]" : text;
    }

    private File getFile() {
        if (fileBasedPayload == null && fileSupplier != null) {
            try {
                File file = fileSupplier.get();
                fileBasedPayload = new FileBasedPayload(file);
            } catch (Exception ignored) { // receiver will get null and react to it
            }
        }
        return fileBasedPayload == null ? null : fileBasedPayload.file();
    }

    private static FileBasedPayload buildFileBasedPayload(File file) {
        return file == null ? null : new FileBasedPayload(file);
    }

    public static TalkResponse ofAnimation(long chatId, File file) {
        return new TalkResponse(MediaType.ANIMATION, null, false,
            buildFileBasedPayload(file), null, null, chatId, null, new ArrayList<>(), NORMAL, null, null,
            null, null, null, null,
            null);
    }

    public static TalkResponse ofAudio(long chatId, File file) {
        return new TalkResponse(MediaType.AUDIO, null, false,
            buildFileBasedPayload(file), null, null, chatId, null, new ArrayList<>(), NORMAL, null, null,
            null, null, null, null,
            null);
    }

    public static TalkResponse ofContact(long chatId, String phoneNumber, String firstName) {
        return new TalkResponse(MediaType.CONTACT, null, false,
            null, new ContactPayload(phoneNumber, firstName), null, chatId, null, new ArrayList<>(), NORMAL, null, null,
            null, null, null, null,
            null);
    }

    public static TalkResponse ofDocument(long chatId, File file) {
        return new TalkResponse(MediaType.DOCUMENT, null, false,
            buildFileBasedPayload(file), null, null, chatId, null, new ArrayList<>(), NORMAL, null, null,
            null, null, null, null,
            null);
    }

    public static TalkResponse ofLocation(long chatId, float latitude, float longitude) {
        return new TalkResponse(MediaType.LOCATION, null, false,
            null, null, new LocationPayload(latitude, longitude), chatId, null, new ArrayList<>(), NORMAL, null, null,
            null, null, null, null,
            null);
    }

    public static TalkResponse ofPhoto(long chatId, File file) {
        return new TalkResponse(MediaType.PHOTO, null, false,
            buildFileBasedPayload(file), null, null, chatId, null, new ArrayList<>(), NORMAL, null, null,
            null, null, null, null,
            null);
    }

    public static TalkResponse ofText(long chatId, String text) {
        return new TalkResponse(MediaType.TEXT, TextEncoder.normalizeAndEncode(text), false,
            null, null, null, chatId, null, new ArrayList<>(), NORMAL, null, null,
            null, null, null, null,
            null);
    }

    public static TalkResponse ofHtml(long chatId, String text) {
        return new TalkResponse(MediaType.TEXT, TextEncoder.normalize(text), true,
            null, null, null, chatId, null, new ArrayList<>(), NORMAL, null, null,
            null, null, null, null,
            null);
    }

    public static TalkResponse ofVideo(long chatId, File file) {
        return new TalkResponse(MediaType.VIDEO, null, false,
            buildFileBasedPayload(file), null, null, chatId, null, new ArrayList<>(), NORMAL, null, null,
            null, null, null, null,
            null);
    }

    public static TalkResponse ofVideoNote(long chatId, File file) {
        return new TalkResponse(MediaType.VIDEO_NOTE, null, false,
            buildFileBasedPayload(file), null, null, chatId, null, new ArrayList<>(), NORMAL, null, null,
            null, null, null, null,
            null);
    }

    public static TalkResponse ofVoice(long chatId, File file) {
        return new TalkResponse(MediaType.VOICE, null, false,
            buildFileBasedPayload(file), null, null, chatId, null, new ArrayList<>(), NORMAL, null, null,
            null, null, null, null,
            null);
    }

    public static TalkResponse ofFlying(long chatId, long messageId) {
        return new TalkResponse(MediaType.ACTION_DELETE_MESSAGE, null, false,
            null, null, null, chatId, messageId, new ArrayList<>(), LOW, null, null,
            null, null, null, null,
            null);

    }

    public TalkResponse with(InputHandler handler) {
        return withIf(handler, true);
    }

    public TalkResponse withIf(InputHandler handler, boolean doAddHandler) {
        if (doAddHandler) {
            handlers.add(handler);
        }
        return this;
    }

    public TalkResponse withString(String classAndMethod, String trigger) {
        return withStringIf(classAndMethod, trigger, true);
    }

    public TalkResponse withStringIf(String classAndMethod, String trigger, boolean doAddHandler) {
        if (doAddHandler) {
            handlers.add(InputHandler.of(classAndMethod, InputTrigger.ofString(trigger)));
        }
        return this;
    }

    public TalkResponse withButton(String classAndMethod, String trigger) {
        return withButtonIf(classAndMethod, trigger, true);
    }

    public TalkResponse withButton(String classAndMethod, String trigger, int row, int column) {
        return withButtonIf(classAndMethod, trigger, row, column, true);
    }

    public TalkResponse withButtonIf(String classAndMethod, String trigger, boolean doAddHandler) {
        if (doAddHandler) {
            handlers.add(InputHandler.of(classAndMethod, InputTrigger.ofString(trigger).asButton()));
        }
        return this;
    }

    public TalkResponse withButtonIf(String classAndMethod, String trigger, int row, int column, boolean doAddHandler) {
        if (doAddHandler) {
            handlers.add(InputHandler.of(classAndMethod, InputTrigger.ofString(trigger).asButton(row, column)));
        }
        return this;
    }

    public TalkResponse withRegex(String classAndMethod, String trigger) {
        return withRegexIf(classAndMethod, trigger, true);
    }

    public TalkResponse withRegexIf(String classAndMethod, String trigger, boolean doAddHandler) {
        if (doAddHandler) {
            handlers.add(InputHandler.of(classAndMethod, InputTrigger.ofRegex(trigger)));
        }
        return this;
    }

    public TalkResponse withMedia(String classAndMethod, MediaType mediaType) {
        return withMediaIf(classAndMethod, mediaType, true);
    }

    public TalkResponse withMediaIf(String classAndMethod, MediaType mediaType, boolean doAddHandler) {
        if (doAddHandler) {
            handlers.add(InputHandler.of(classAndMethod, InputTrigger.ofMedia(mediaType)));
        }
        return this;
    }

    public TalkResponse withHandlers(List<InputHandler> handlers) {
        this.handlers.addAll(handlers);
        return this;
    }

    public TalkResponse withPreserveHandlers() {
        this.handlers = Talk.preserveHandlers();
        return this;
    }

    public TalkResponse withEmptyHandlers() {
        return withHandlers(emptyList());
    }

    public TalkResponse withLowPriority() {
        this.priority = LOW;
        return this;
    }

    public TalkResponse withFlying(int duration, TimeUnit timeUnit) {
        if (isFlyingEnabled()) {
            this.flyTime = Math.min(TimeUnit.MILLISECONDS.convert(duration, timeUnit), getFlyingMessagesMaxTTL());
        }
        return this;
    }

    public TalkResponse withFlying() {
        if (isFlyingEnabled()) {
            this.flyTime = Math.min(getFlyingMessagesDefaultTTL(), getFlyingMessagesMaxTTL());
        }
        return this;
    }

    public TalkResponse withTextSupplier(Supplier<String> textSupplier) {
        this.textSupplier = textSupplier;
        return this;
    }

    public TalkResponse withFileSupplier(Supplier<File> fileSupplier) {
        this.fileSupplier = fileSupplier;
        return this;
    }

    public TalkResponse withExceptionMapper(Function<Exception, TalkResponse> mapper) {
        this.exceptionMapper = mapper;
        return this;
    }

    public TalkResponse next(TalkResponse response) {
        this.nextResponse = response;
        return this;
    }

    public TalkResponse getLastResponseInChain() {
        TalkResponse lastResponse = this;
        while (lastResponse.nextResponse != null) {
            lastResponse = lastResponse.nextResponse;
        }
        return lastResponse;
    }

    public void addDecoration(Consumer<AbstractSendRequest<?>> decorator) {
        decorators.add(decorator);
    }

    public List<TalkResponse> getFinalResponses() {
        if (mediaType == MediaType.TEXT && text != null && isHtml) {
            TalkResponse response = new TalkResponse(
                MediaType.TEXT,
                text,
                true,
                null,
                null,
                null,
                chatId,
                null,
                getHandlers(),
                priority,
                flyTime,
                null,
                null,
                null,
                null,
                null,
                null
            );
            decorateButtons(response);
            return singletonList(response);
        }

        if (mediaType != MediaType.TEXT || text == null) {
            return singletonList(this);
        }

        List<TalkResponse> finalResponses = new ArrayList<>();
        List<String> chunks = splitText(text);
        for (String chunk : chunks) {
            finalResponses.add(new TalkResponse(
                MediaType.TEXT,
                chunk,
                false,
                null,
                null,
                null,
                chatId,
                null,
                getHandlers(),
                priority,
                flyTime,
                null,
                null,
                null,
                null,
                null,
                null
            ));
        }
        for (int i = 0; i < finalResponses.size(); i++) {
            TalkResponse finalResponse = finalResponses.get(i);
            if (i == finalResponses.size() - 1) {
                decorateButtons(finalResponse);
            }
        }

        return finalResponses;
    }

    public AbstractSendRequest<?> getTelegramSendRequest() {
        if (telegramSendRequest == null) {
            telegramSendRequest = buildTelegramSendRequest();
        }
        return telegramSendRequest;
    }

    private AbstractSendRequest<?> buildTelegramSendRequest() {
        AbstractSendRequest<?> telegramSendRequest;

        switch (mediaType) {
            case TEXT:
                String unencodedText = TextEncoder.unencode(getText());
                telegramSendRequest = isHtml ?
                    doBuildTelegramSendRequest(unencodedText, text -> new SendMessage(chatId, unencodedText).parseMode(ParseMode.HTML)) :
                    doBuildTelegramSendRequest(unencodedText, text -> new SendMessage(chatId, unencodedText));
                break;
            case CONTACT:
                telegramSendRequest = new SendContact(chatId, contactPayload.phoneNumber, contactPayload.firstName);
                break;
            case LOCATION:
                telegramSendRequest = new SendLocation(chatId, locationPayload.latitude, locationPayload.longitude);
                break;
            case ANIMATION:
                telegramSendRequest = doBuildTelegramSendRequest(getFile(), file -> new SendAnimation(chatId, file));
                break;
            case AUDIO:
                telegramSendRequest = doBuildTelegramSendRequest(getFile(), file -> new SendAudio(chatId, file));
                break;
            case DOCUMENT:
                telegramSendRequest = doBuildTelegramSendRequest(getFile(), file -> new SendDocument(chatId, file));
                break;
            case PHOTO:
                telegramSendRequest = doBuildTelegramSendRequest(getFile(), file -> new SendPhoto(chatId, file));
                break;
            case VIDEO:
                telegramSendRequest = doBuildTelegramSendRequest(getFile(), file -> new SendVideo(chatId, file));
                break;
            case VIDEO_NOTE:
                telegramSendRequest = doBuildTelegramSendRequest(getFile(), file -> new SendVideoNote(chatId, file));
                break;
            case VOICE:
                telegramSendRequest = doBuildTelegramSendRequest(getFile(), file -> new SendVoice(chatId, file));
                break;
            default:
                telegramSendRequest = null;
        }

        if (telegramSendRequest != null) {
            for (Consumer<AbstractSendRequest<?>> decorator : decorators) {
                decorator.accept(telegramSendRequest);
            }
        }

        return telegramSendRequest;
    }

    private AbstractSendRequest<?> doBuildTelegramSendRequest(String text, Function<String, AbstractSendRequest<?>> telegramSendRequestSupplier) {
        return text == null ? null : telegramSendRequestSupplier.apply(text);
    }

    private AbstractSendRequest<?> doBuildTelegramSendRequest(File file, Function<File, AbstractSendRequest<?>> telegramSendRequestSupplier) {
        return file == null ? null : telegramSendRequestSupplier.apply(file);
    }

    public DeleteMessage getTelegramDeleteMessageRequest() {
        if (messageId == null) {
            throw new RuntimeException("messageId not set for DeleteMessage action");
        }

        int messageIdToFly = messageId.intValue();
        return new DeleteMessage(chatId, messageIdToFly);
    }

    private List<String> splitText(String text) {
        List<String> chunks = splitText("\n\n", singletonList(text));
        chunks = splitText("\n", chunks);
        chunks = splitText(" ", chunks);
        return chunks;
    }

    private List<String> splitText(String separator, List<String> texts) {
        if (allTextsAreShort(texts)) {
            return texts;
        }

        List<String> chunks = new ArrayList<>();
        for (String text : texts) {
            chunks.addAll(splitText(separator, text));
        }
        return chunks;
    }

    private List<String> splitText(String separator, String text) {
        if (text.length() <= MAX_TEXT_MESSAGE_LENGTH) {
            return singletonList(text);
        }

        List<String> chunks = new ArrayList<>();
        String chunk = "";
        for (String part : text.split(separator)) {
            if (! chunk.isEmpty() && chunk.length() + separator.length() + part.length() > MAX_TEXT_MESSAGE_LENGTH) {
                chunks.add(chunk);
                chunk = "";
            }
            chunk += (chunk.isEmpty() ? "" : separator) + part;
        }
        if (! chunk.isEmpty()) {
            chunks.add(chunk);
        }
        return chunks;
    }

    private boolean allTextsAreShort(List<String> texts) {
        for (String text : texts) {
            if (text.length() > MAX_TEXT_MESSAGE_LENGTH) {
                return false;
            }
        }
        return true;
    }

    private void decorateButtons(TalkResponse response) {
        ButtonDecorator.getInstance().decorate(response);
    }

    private static long getFlyingMessagesDefaultTTL() {
        return TimeUnit.MILLISECONDS.convert(LibSetting.getInteger(LibSetting.FLYING_MESSAGES_DURATION_IN_MINUTES), TimeUnit.MINUTES);
    }

    private static long getFlyingMessagesMaxTTL() {
        return TimeUnit.MILLISECONDS.convert(LibSetting.getInteger(LibSetting.FLYING_MESSAGES_MAX_DURATION_IN_MINUTES), TimeUnit.MINUTES);
    }

    private static boolean isFlyingEnabled() {
        return getFlyingMessagesDefaultTTL() != -1;
    }
}
