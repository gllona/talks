package org.logicas.librerias.talks.engine;

import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.Update;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.logicas.librerias.talks.api.InputAdapterApi;
import org.logicas.librerias.talks.config.LibSetting;

import java.util.concurrent.TimeUnit;

@RequiredArgsConstructor
@EqualsAndHashCode
@Getter
public class TalkRequest {

    final private InputAdapterApi inputAdapter;
    final private Update update;
    @Setter @EqualsAndHashCode.Exclude private InputMatch match;
    @EqualsAndHashCode.Exclude private Long flyTime;
    @Setter private boolean isJoinGroup;
    @Setter private boolean isLeaveGroup;

    public Long getChatId() {
        if (update.message() != null) {
            return update.message().chat().id();
        } else if (update.editedMessage() != null) {
            return update.editedMessage().chat().id();
        } else if (update.channelPost() != null) {
            return update.channelPost().chat().id();
        } else if (update.editedChannelPost() != null) {
            return update.editedChannelPost().chat().id();
        }
        return null;
    }

    public Message getMessage() {
        return update.message() != null ? update.message() : (
            update.editedMessage() != null ? update.editedMessage() : (
                update.channelPost() != null ? update.channelPost() :
                update.editedChannelPost()
        ));
    }

    public Long getFlyTime() {
        return flyTime;
    }

    public void fly(int duration, TimeUnit timeUnit) {
        if (isFlyingEnabled()) {
            this.flyTime = Math.min(TimeUnit.MILLISECONDS.convert(duration, timeUnit), getFlyingMessagesMaxTTL());
        }
    }

    public void fly() {
        if (isFlyingEnabled()) {
            this.flyTime = Math.min(getFlyingMessagesDefaultTTL(), getFlyingMessagesMaxTTL());
        }
    }

    private static long getFlyingMessagesDefaultTTL() {
        return TimeUnit.MILLISECONDS.convert(LibSetting.getInteger(LibSetting.FLYING_MESSAGES_DURATION_IN_MINUTES), TimeUnit.MINUTES);
    }

    private static long getFlyingMessagesMaxTTL() {
        return TimeUnit.MILLISECONDS.convert(LibSetting.getInteger(LibSetting.FLYING_MESSAGES_MAX_DURATION_IN_MINUTES), TimeUnit.MINUTES);
    }

    private static boolean isFlyingEnabled() {
        return getFlyingMessagesDefaultTTL() != -1;
    }
}
