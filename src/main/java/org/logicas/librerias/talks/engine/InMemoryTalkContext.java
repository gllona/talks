package org.logicas.librerias.talks.engine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import static java.util.Collections.emptyList;

public class InMemoryTalkContext extends TalkContext {

    private static class SessionContext {
        private static Object NULL = new Object();
        private Map<String, Object> vars = new ConcurrentHashMap<>();
        public void set(String name, Object value) {
            vars.put(name, value == null ? NULL : value);
        }
        public void remove(String name) {
            vars.remove(name);
        }
        public Object get(String name) {
            Object value = vars.get(name);
            return value == NULL ? null : value;
        }
        public Map<String, Object> getAll() { return vars; }
        public void clear() {
            vars.clear();
        }
    }

    private SessionContext globalVars = new SessionContext();
    private Map<Long, SessionContext> boundedVars = new ConcurrentHashMap<>();
    private Collection<InputHandler> globalHandlers = new ConcurrentLinkedQueue<>();
    private Map<Long, Collection<InputHandler>> boundedHandlers = new ConcurrentHashMap<>();

    @Override
    protected List<InputHandler> getGlobalHandlers() {
        return new ArrayList<>(globalHandlers);
    }

    @Override
    protected List<InputHandler> getBoundedHandlers(long session) {
        return new ArrayList<>(boundedHandlers.get(session) == null ?
            emptyList() :
            new ArrayList<>(boundedHandlers.get(session))
        );
    }

    @Override
    protected void deleteBoundedHandlers(long session) {
        boundedHandlers.get(session).clear();
    }

    @Override
    public void setGlobalHandlers(List<InputHandler> handlers) {
        globalHandlers.clear();
        globalHandlers.addAll(handlers);
    }

    @Override
    public void setHandlers(long session, List<InputHandler> handlers) {
        if (handlers == null || Talk.preserveHandlers().equals(handlers)) {
            return;
        }
        boundedHandlers.putIfAbsent(session, new ConcurrentLinkedQueue<>());
        boundedHandlers.get(session).clear();
        this.boundedHandlers.get(session).addAll(handlers);
    }

    @Override
    public void setGlobal(String name, Object value) {
        globalVars.set(name, value);
    }

    @Override
    public Object getGlobal(String name) {
        return globalVars.get(name);
    }

    @Override
    public void set(long session, String name, Object value) {
        boundedVars.putIfAbsent(session, new SessionContext());
        boundedVars.get(session).set(name, value);
    }

    @Override
    public void remove(long session, String name) {
        boundedVars.putIfAbsent(session, new SessionContext());
        boundedVars.get(session).remove(name);
    }

    @Override
    public Object get(long session, String name) {
        boundedVars.putIfAbsent(session, new SessionContext());
        return boundedVars.get(session).get(name);
    }

    @Override
    public Map<String, Object> getAll(long session) {
        boundedVars.putIfAbsent(session, new SessionContext());
        return boundedVars.get(session).getAll();
    }

    @Override
    public void reset(long session) {
        if (boundedVars.get(session) != null) {
            boundedVars.get(session).clear();
        }
        if (boundedHandlers.get(session) != null) {
            boundedHandlers.get(session).clear();
        }
    }

    @Override
    public void flush(long session) {
        // do nothing
    }

    @Override
    protected void forget(long session) {
        throw new UnsupportedOperationException("Forgetting the user context is not implemented for in memory context");
    }
}
