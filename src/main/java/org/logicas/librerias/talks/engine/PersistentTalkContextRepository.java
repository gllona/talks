package org.logicas.librerias.talks.engine;

import org.logicas.librerias.copersistance.Dao;
import org.logicas.librerias.copersistance.repositories.SingleKeyEntityRepository;
import org.logicas.librerias.talks.api.TalkType;

import java.util.List;

import static java.util.Collections.emptyList;

public class PersistentTalkContextRepository extends SingleKeyEntityRepository<PersistentTalkContextEntry> {

    private static PersistentTalkContextRepository instance;

    private Dao dao;

    private PersistentTalkContextRepository(Dao dao) {
        this.dao = dao;
    }

    public static synchronized PersistentTalkContextRepository getInstance(Dao dao) {
        if (instance == null) {
            instance = new PersistentTalkContextRepository(dao);
        }
        return instance;
    }

    private static final String TABLE = "talk_contexts";

    @Override
    protected Dao dao() {
        return dao;
    }

    @Override
    public String tableName() {
        return TABLE;
    }

    @Override
    protected String[] columnNames() {
        return PersistentTalkContextEntry.FIELDS;
    }

    @Override
    public String idFieldName() {
        return PersistentTalkContextEntry.FIELD_ID;
    }

    public PersistentTalkContextEntry readHandlersEntry(TalkType talkType, long session) {
        PersistentTalkContextEntry entry = readOne(String.format(
            "session_id = %d AND talk_name = '%s' AND type = '%s'",
            session, talkType.getName(), PersistentTalkContextEntry.TYPE_INPUT_HANDLERS
        ));
        return entry;
    }

    public List<InputHandler> readHandlers(TalkType talkType, long session) {
        PersistentTalkContextEntry entry = readHandlersEntry(talkType, session);
        if (entry == null) {
            return emptyList();
        }
        List<InputHandler> handlers = entry.getInputHandlers();
        return handlers;
    }

    public void deleteHandlers(TalkType talkType, long session) {
        deleteByCondition(String.format(
            "session_id = %d AND talk_name = '%s' AND type = '%s'",
            session, talkType.getName(), PersistentTalkContextEntry.TYPE_INPUT_HANDLERS
        ));
    }

    public void deleteHandlers(long session) {
        deleteByCondition(String.format(
            "session_id = %d AND type = '%s'",
            session, PersistentTalkContextEntry.TYPE_INPUT_HANDLERS
        ));
    }

    public PersistentTalkContextEntry readVarsEntry(TalkType talkType, long session) {
        PersistentTalkContextEntry entry = readOne(String.format(
            "session_id = %d AND talk_name = '%s' AND type = '%s'",
            session, talkType.getName(), PersistentTalkContextEntry.TYPE_VARS
        ));
        return entry;
    }

    public void deleteVars(TalkType talkType, long session) {
        deleteByCondition(String.format(
            "session_id = %d AND talk_name = '%s' AND type = '%s'",
            session, talkType.getName(), PersistentTalkContextEntry.TYPE_VARS
        ));
    }

    public void deleteVars(long session) {
        deleteByCondition(String.format(
            "session_id = %d AND type = '%s'",
            session, PersistentTalkContextEntry.TYPE_VARS
        ));
    }
}
