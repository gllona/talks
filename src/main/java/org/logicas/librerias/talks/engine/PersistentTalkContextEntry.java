package org.logicas.librerias.talks.engine;

import com.google.gson.reflect.TypeToken;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.logicas.librerias.copersistance.entities.SingleKeyEntity;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Setter
@Getter
@ToString
public class PersistentTalkContextEntry extends SingleKeyEntity {

    public static final String FIELD_ID = "id";
    public static final String FIELD_SESSION_ID = "session_id";
    public static final String FIELD_TALK_NAME = "talk_name";
    public static final String FIELD_TYPE = "type";
    public static final String FIELD_CONTENT = "content";

    public static final String TYPE_INPUT_HANDLERS = "handlers";
    public static final String TYPE_VARS = "vars";

    public static final String[] FIELDS = {
        FIELD_ID,
        FIELD_SESSION_ID,
        FIELD_TALK_NAME,
        FIELD_TYPE,
        FIELD_CONTENT
    };

    private static final String[] KEYS = {
        FIELD_ID
    };

    private Long id;
    Long sessionId;
    private String talkName;
    String type;
    private String content;

    public PersistentTalkContextEntry(Long sessionId, String talkName, String type) {
        this.sessionId = sessionId;
        this.talkName = talkName;
        this.type = type;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Map<String, Object> parameters() {
        Map<String, Object> params = new HashMap<>();

        conditionalAddToParams(id, FIELD_ID, params);
        conditionalAddToParams(sessionId, FIELD_SESSION_ID, params);
        conditionalAddToParams(talkName, FIELD_TALK_NAME, params);
        conditionalAddToParams(type, FIELD_TYPE, params);
        conditionalAddToParams(content, FIELD_CONTENT, params);

        return params;
    }

    @Override
    public String[] getKeyColumns() {
        return KEYS;
    }

    public void setInputHandlers(Collection<InputHandler> inputHandlers) {
        type = TYPE_INPUT_HANDLERS;
        content = gson.toJson(inputHandlers);
    }

    public void setVars(Map<String, Object> vars) {
        type = TYPE_VARS;
        content = gson.toJson(vars);
    }

    public List<InputHandler> getInputHandlers() {
        if (! TYPE_INPUT_HANDLERS.equals(type)) {
            throw new RuntimeException("getInputHandlers() can not be invoked for PersistentTalkContextEntry with type " + type);
        }
        Type type = new TypeToken<List<InputHandler>>(){}.getType();
        List<InputHandler> handlers = gson.fromJson(content, type);
        return handlers;
    }

    public Map<String, Object> getVars() {
        if (! TYPE_VARS.equals(type)) {
            throw new RuntimeException("getVars() can not be invoked for PersistentTalkContextEntry with type " + type);
        }
        if (content == null) {
            return new HashMap<>();
        }
        Type type = new TypeToken<HashMap<String, Object>>(){}.getType();
        Map<String, Object> vars = gson.fromJson(content, type);
        return vars;
    }
}
