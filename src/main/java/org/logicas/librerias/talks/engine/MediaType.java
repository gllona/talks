package org.logicas.librerias.talks.engine;

import com.pengrad.telegrambot.model.Message;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum MediaType {

    ANIMATION(Message::animation),
    AUDIO(Message::audio),
    CONTACT(Message::contact),
    DOCUMENT(Message::document),
    LOCATION(Message::location),
    PHOTO(Message::photo),
    TEXT(MediaType::extractText),
    VIDEO(Message::video),
    VIDEO_NOTE(Message::videoNote),
    VOICE(Message::voice),
    ACTION_DELETE_MESSAGE(null);
    //GAME(Message::game),
    //STICKER(Message::sticker),
    //CAPTION(Message::caption),
    //VENUE(Message::venue),
    //POLL(Message::poll),

    @FunctionalInterface
    public interface TypeExtractor {
        Object extract(Message message);
    }

    private final TypeExtractor extractor;

    public Object extract(Message message) {
        return extractor.extract(message);
    }

    //TODO get editedText() if text() not available
    private static String extractText(Message message) {
        return TextEncoder.normalizeAndEncode(message.text());
    }
}
