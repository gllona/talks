package org.logicas.librerias.talks.engine;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.config.LibSetting;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public abstract class TalkContext {

    private static Logger logger = LogManager.getLogger(TalkContext.class);

    private static Map<String, TalkContext> contexts = new ConcurrentHashMap<>();

    protected Set<Talk> supportedTalks = new HashSet<>();

    public static TalkContext create(TalksConfiguration talksConfig, Talk talk) {
        TalkContext context = talksConfig.getTalkContext(talk);
        context.supportedTalks.add(talk);
        contexts.put(talk.getClass().getSimpleName(), context);
        return context;
    }

    public static TalkContext create(String useContextFromThisClassName, Talk talk) {
        TalkContext context = contexts.get(useContextFromThisClassName);
        return doCreate(useContextFromThisClassName, talk, context);
    }

    public static TalkContext create(Class<? extends Talk> useContextFromThisClass, Talk talk) {
        TalkContext context = contexts.get(useContextFromThisClass.getSimpleName());
        return doCreate(useContextFromThisClass.getSimpleName(), talk, context);
    }

    private static TalkContext doCreate(String useContextFromThisClassName, Talk talk, TalkContext context) {
        if (context == null) {
            throw new RuntimeException(String.format(
                "TalkContext for [%s] must be created before using it for [%s]",
                useContextFromThisClassName, talk.getClass().getSimpleName()
            ));
        }
        context.supportedTalks.add(talk);
        return context;
    }

    public abstract void setGlobalHandlers(List<InputHandler> handlers);

    public abstract void setHandlers(long session, List<InputHandler> handlers);

    public abstract void setGlobal(String name, Object value);

    public abstract Object getGlobal(String name);

    public abstract void set(long session, String name, Object value);

    public abstract void remove(long session, String name);

    public abstract Object get(long session, String name);

    public abstract Map<String, Object> getAll(long session);

    public String getString(long session, String name) {
        Object value = get(session, name);
        return value == null ? null : value.toString();
    }

    public Long getLong(long session, String name) {
        Object value = get(session, name);
        return value == null ? null : Double.valueOf(value.toString()).longValue();
    }

    public Integer getInt(long session, String name) {
        Object value = get(session, name);
        return value == null ? null : Double.valueOf(value.toString()).intValue();
    }

    public Double getDouble(long session, String name) {
        Object value = get(session, name);
        return value == null ? null : Double.parseDouble(value.toString());
    }

    public abstract void reset(long session);

    public abstract void flush(long session);

    public void forgetPersonalData(long session) {
        reset(session);
        forget(session);
    }

    protected abstract void forget(long session);

    protected abstract List<InputHandler> getGlobalHandlers();

    protected abstract List<InputHandler> getBoundedHandlers(long session);

    protected abstract void deleteBoundedHandlers(long session);

    public Optional<InputMatch> match(TalkRequest call) {
        List<InputHandler> globalHandlers = getGlobalHandlers();
        List<InputHandler> boundedHandlers = getBoundedHandlers(call.getChatId());

        boolean isStartMessage = call.getMessage() != null && call.getMessage().text() != null &&
            getStartMessage().equalsIgnoreCase(call.getMessage().text());

        Optional<InputMatch> match = match(call, globalHandlers, isStartMessage);
        if (match.isPresent()) {
            deleteBoundedHandlers(call.getChatId());
            return match;
        }
        if (! isStartMessage && boundedHandlers != null) {
            match = match(call, boundedHandlers, true);
        }
        if (match.isPresent()) {
            return match;
        }
        match = match(call, globalHandlers, true);
        return match;
    }

    private Optional<InputMatch> match(TalkRequest call, Collection<InputHandler> handlers, boolean includeRegexTriggers) {
        for (InputHandler handler : handlers) {
            if (handler == null) {
                logger.warn(String.format(
                    "A [null] handler was found: [message=%s] [includeRegexTriggers=%b] [handlers=%s]",
                    call.getMessage(), includeRegexTriggers, handlers.toString()));
                continue;
            }
            if (! supportsHandler(handler)) {
                continue;
            }
            Optional<InputMatch> matches = handler.matches(call, includeRegexTriggers);
            if (matches.isPresent()) {
                return matches;
            }
        }
        return Optional.empty();
    }

    private boolean supportsHandler(InputHandler handler) {
        for (Talk talk : supportedTalks) {
            if (talk.getClass().getName().endsWith(handler.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private String getStartMessage() {
        return LibSetting.get(LibSetting.START_MESSAGE);
    }
}
