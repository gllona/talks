package org.logicas.librerias.talks.engine;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.logicas.librerias.copersistance.entities.SingleKeyEntity;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@NoArgsConstructor
@Setter
@Getter
@ToString
public class Interaction extends SingleKeyEntity {

    public static final String FIELD_ID = "id";
    public static final String FIELD_TYPE = "type";
    public static final String FIELD_CHAT_ID = "chat_id";
    public static final String FIELD_MESSAGE_ID = "message_id";
    public static final String FIELD_CREATED_AT = "created_at";
    public static final String FIELD_FLY_AT = "fly_at";

    public static final String TYPE_REQUEST = "request";
    public static final String TYPE_RESPONSE = "response";

    public static final String[] FIELDS = {
        FIELD_ID,
        FIELD_TYPE,
        FIELD_CHAT_ID,
        FIELD_MESSAGE_ID,
        FIELD_CREATED_AT,
        FIELD_FLY_AT
    };

    private static final String[] KEYS = {
        FIELD_ID
    };

    private Long id;
    private String type;
    private Long chatId;
    private Long messageId;
    private LocalDateTime createdAt;
    private LocalDateTime flyAt;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getChatId() {
        return chatId;
    }

    public void setChatId(Long chatId) {
        this.chatId = chatId;
    }

    public Long getMessageId() {
        return messageId;
    }

    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getFlyAt() {
        return flyAt;
    }

    public void setFlyAt(LocalDateTime flyAt) {
        this.flyAt = flyAt;
    }

    @Override
    public Map<String, Object> parameters() {
        Map<String, Object> params = new HashMap<>();

        conditionalAddToParams(id, FIELD_ID, params);
        conditionalAddToParams(type, FIELD_TYPE, params);
        conditionalAddToParams(chatId, FIELD_CHAT_ID, params);
        conditionalAddToParams(messageId, FIELD_MESSAGE_ID, params);
        conditionalAddToParams(createdAt, FIELD_CREATED_AT, params);
        conditionalAddToParams(flyAt, FIELD_FLY_AT, params);

        return params;
    }

    @Override
    public String[] getKeyColumns() {
        return KEYS;
    }
}
