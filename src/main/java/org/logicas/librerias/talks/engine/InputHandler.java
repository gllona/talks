package org.logicas.librerias.talks.engine;

import static java.util.Arrays.asList;

import java.util.List;
import java.util.Optional;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@EqualsAndHashCode
@Getter
public class InputHandler {

    // not more used after changing handlers definitions in Talks from method to string
    @FunctionalInterface
    public interface TalkMethod {
        TalkResponse work(TalkRequest call, List<String> matches);
    }

    private List<InputTrigger> triggers;
    private String className;
    private String methodName;

    public static InputHandler of(String classAndMethod, InputTrigger... triggers) {
        int separatorIndex = classAndMethod.indexOf("::");
        if (separatorIndex == -1) {
            throw new RuntimeException("Class&Method separator not found in " + classAndMethod);
        }
        String className = classAndMethod.substring(0, separatorIndex);
        String methodName = classAndMethod.substring(separatorIndex + 2);
        if (className.isEmpty() || methodName.isEmpty()) {
            throw new RuntimeException("Invalid Class&Method: " + classAndMethod);
        }
        return new InputHandler(asList(triggers), className, methodName);
    }

    public Optional<InputMatch> matches(TalkRequest call, boolean includeRegexTriggers) {
        for (InputTrigger trigger : triggers) {
            if (! includeRegexTriggers && trigger.isRegex()) {
                continue;
            }
            Optional<InputMatch> match = trigger.matches(call);
            if (match.isPresent()) {
                match.get().setHandler(this);
                return match;
            }
        }
        return Optional.empty();
    }
}
