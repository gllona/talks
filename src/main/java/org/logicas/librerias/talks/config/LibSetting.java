package org.logicas.librerias.talks.config;

public enum LibSetting {
    MESSAGES_SENDER_NUM_THREADS, // ignored for RatedPersistentMessageSenderV3
    USE_SINGLE_THREAD,
    SHUTDOWN_TIMEOUT_IN_MILLIS,
    TALK_PACKAGE_NAME,
    START_MESSAGE,
    TELEGRAM_GET_UPDATES_LIMIT,
    CHAT_INTER_MESSAGE_DELAY_IN_MILLIS,
    ALL_CHATS_MAX_MESSAGES_PER_SECOND,
    BOT_NAME,
    TELEGRAM_API_KEY,
    FILE_DOWNLOAD_PATH,
    DB_RDBMS,
    DB_HOST,
    DB_PORT,
    DB_DATABASE,
    DB_USERNAME,
    DB_PASSWORD,
    DB_OPTIONS,
    DB_CONNECTION_POOL_SIZE,
    DB_CONNECTION_POOL_TIMEOUT_IN_MILLIS,
    DB_CONNECTION_TTL_IN_MILLIS,
    DB_KEEPALIVE_QUERY,
    SESSIONS_TO_LOG,
    PERSONAL_DATA_REMOVAL_DELAY_IN_MILLIS,
    FALLBACK_RESPONSE,
    USE_PERSISTENT_MESSAGE_SENDER,
    PERSISTENT_MESSAGE_SENDER_VERSION,
    FLYING_MESSAGES_DURATION_IN_MINUTES,
    FLYING_MESSAGES_MAX_DURATION_IN_MINUTES,
    FLYING_MESSAGES_STEALTH_FACTOR,
    ENCODE_EMOJI;

    public static String get(LibSetting setting) {
        return LibConfig.getInstance().getString(setting);
    }

    public static int getInteger(LibSetting setting) {
        return Integer.parseInt(get(setting));
    }
}
