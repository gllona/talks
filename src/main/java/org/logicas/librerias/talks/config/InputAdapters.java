package org.logicas.librerias.talks.config;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.logicas.librerias.talks.api.InputAdapterApi;
import org.logicas.librerias.talks.telegram.TelegramInputAdapter;

@AllArgsConstructor
@Getter
public enum InputAdapters {

    TELEGRAM(TelegramInputAdapter.class);

    public Class<? extends InputAdapterApi> adapter;
}
