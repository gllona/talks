package org.logicas.librerias.talks.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.logicas.librerias.talks.api.ConfigInterceptorApi;

import javax.management.AttributeNotFoundException;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.function.Supplier;
import java.util.regex.Pattern;

public class LibConfig {

    private static Pattern regex = Pattern.compile("(?<!\\$)\\$\\{([A-Za-z0-9_]+)\\}");
    private static Logger logger = LogManager.getLogger(LibConfig.class);
    private static LibConfig instance = getInstance();

    private Properties properties;
    private ConfigInterceptorApi interceptor;

    private LibConfig() throws IOException, AttributeNotFoundException {
        readProperties();
        checkProperties();
    }

    public static synchronized LibConfig getInstance() {
        if (instance == null) {
            try {
                instance = new LibConfig();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        return instance;
    }

    private void readProperties() throws IOException {
        FileInputStream fis = new FileInputStream(Constants.PROPERTIES_FILE);
        Properties tmpProperties = new Properties();
        tmpProperties.load(fis);
        properties = interpolateEnvironmentVariables(tmpProperties);
    }

    private Properties interpolateEnvironmentVariables(Properties inProperties) {
        Properties tmpProperties = new Properties();
        for (Map.Entry<Object, Object> property : inProperties.entrySet()) {
            String propertyKey = property.getKey().toString();
            String inPropertyValue = property.getValue().toString();
            String outPropertyValue = interpolateEnvironmentVariables(inPropertyValue);
            tmpProperties.setProperty(propertyKey, outPropertyValue);
        }
        return tmpProperties;
    }

    private String interpolateEnvironmentVariables(String inPropertyValue) {
        String outPropertyValue = inPropertyValue;
        for (Map.Entry<String, String> envVar : System.getenv().entrySet()) {
            String envVarName = envVar.getKey();
            String envVarValue = envVar.getValue();
            outPropertyValue = interpolateEnvironmentVariable(outPropertyValue, envVarName, envVarValue);
        }
        return outPropertyValue;
    }

    private String interpolateEnvironmentVariable(String inPropertyValue, String envVarName, String envVarValue) {
        String outPropertyValue = regex.matcher(inPropertyValue).replaceAll(mr -> {
            String possibleEnvVarName = mr.group(1);
            return (possibleEnvVarName != null && possibleEnvVarName.equals(envVarName)) ? envVarValue : "\\${" + possibleEnvVarName + "}";
        });
        return outPropertyValue;
    }

    public void checkProperties() throws AttributeNotFoundException {
        logger.info("Checking all properties are set....");
        for (LibSetting setting : LibSetting.values()) {
            String value = properties.getProperty(setting.name());
            if (value == null) {
                logger.error(String.format("Property [%s] is not set!!", setting.name()));
                throw new AttributeNotFoundException(setting.name());
            }
        }
    }

    public synchronized void setInterceptor(ConfigInterceptorApi configInterceptor) {
        this.interceptor = configInterceptor;
    }

    public String getString(LibSetting setting) {
        String settingName = setting.name();
        Supplier<String> defaultSupplier = () -> properties.getProperty(settingName);

        return interceptor == null ?
            defaultSupplier.get() :
            interceptor.getProperty(settingName, defaultSupplier);
    }
}
