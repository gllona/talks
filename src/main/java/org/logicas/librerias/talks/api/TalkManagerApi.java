package org.logicas.librerias.talks.api;

import org.logicas.librerias.talks.engine.Talk;
import org.logicas.librerias.talks.engine.TalkRequest;
import org.logicas.librerias.talks.engine.TalkResponse;

import java.util.Optional;

public interface TalkManagerApi {

    Optional<TalkResponse> dispatch(TalkRequest call);

    // invocation assumes Talks enum elements are same name as their classes
    Talk talkForName(String simpleClassName);

    void resetTalkContexts(long session);

    void flushTalkContexts(long chatId);

    void shutDown();
}
