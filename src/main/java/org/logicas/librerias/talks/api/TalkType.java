package org.logicas.librerias.talks.api;

import org.logicas.librerias.talks.engine.Talk;

public interface TalkType {

    Class<? extends Talk> getTalkClass();

    String getName();
}
