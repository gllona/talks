package org.logicas.librerias.talks.api;

import com.pengrad.telegrambot.model.Update;
import org.logicas.librerias.talks.engine.TalkResponse;

import java.util.List;
import java.util.Optional;

public interface ModelApi {

    void shutdown();

    Optional<TalkResponse> receiveMessage(
        InputAdapterApi inputAdapter,
        Update update
    );

    default void beforeReceiveMessages(
        InputAdapterApi inputAdapter,
        List<Update> updates
    ) {
        // nothing to do
    }

    default void afterReceiveMessages(
        InputAdapterApi inputAdapter,
        List<Update> updates
    ) {
        // nothing to do
    }
}
