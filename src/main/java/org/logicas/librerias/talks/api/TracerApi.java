package org.logicas.librerias.talks.api;

public interface TracerApi {

    void info(Long session, String message);

    void warn(Long session, String message);

    void error(Long session, String message);

    void error(Long session, String message, Throwable e);
}
