package org.logicas.librerias.talks.tracing;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.logicas.librerias.talks.api.TracerApi;
import org.logicas.librerias.talks.config.LibSetting;

import java.util.Arrays;
import java.util.List;

public class SimpleTracer implements TracerApi {

    private static SimpleTracer instance;
    private static Logger logger = LogManager.getLogger(SimpleTracer.class);

    public static synchronized TracerApi getInstance() {
        if (instance == null) {
            instance = new SimpleTracer();
        }
        return instance;
    }

    @Override
    public void info(Long session, String message) {
        if (shouldLog(session)) {
            logger.info(messageFor(session, message));
        }
    }

    @Override
    public void warn(Long session, String message) {
        if (shouldLog(session)) {
            logger.warn(messageFor(session, message));
        }
    }

    @Override
    public void error(Long session, String message) {
        if (shouldLog(session)) {
            logger.error(messageFor(session, message));
        }
    }

    @Override
    public void error(Long session, String message, Throwable t) {
        if (shouldLog(session)) {
            logger.error(messageFor(session, message), t);
        }
    }

    private boolean shouldLog(Long session) {
        List<String> sessionsToLog = getSessionsToLog();
        return session == null ||
            sessionsToLog.contains("*") ||
            sessionsToLog.contains(session.toString());
    }

    private String messageFor(Long session, String message) {
        return String.format("[%d] %s", session, message);
    }

    private static List<String> getSessionsToLog() {
        return Arrays.asList(
            LibSetting.get(LibSetting.SESSIONS_TO_LOG).split(",")
        );
    }
}
