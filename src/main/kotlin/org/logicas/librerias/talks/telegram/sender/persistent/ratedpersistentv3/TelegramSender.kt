package org.logicas.librerias.talks.telegram.sender.persistent.ratedpersistentv3

import com.google.common.cache.CacheBuilder
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.isActive
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import org.apache.logging.log4j.LogManager
import org.logicas.librerias.talks.Coroutines
import org.logicas.librerias.talks.api.TalksConfiguration
import org.logicas.librerias.talks.config.LibSetting
import org.logicas.librerias.talks.telegram.sender.persistent.PersistentMessageRepository
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicBoolean

class TelegramSender(
    val talksConfig: TalksConfiguration
) {
    var processor: Job? = null
    val startMutex = Mutex()
    val started = AtomicBoolean(false)
    val rendezvousQueue = RendezvousQueue.getInstance(talksConfig)
    val failedMessageQueue = Channel<Message>(Channel.UNLIMITED)
    val messagesSentInLastSecondCache = CacheBuilder.newBuilder().expireAfterWrite(1, TimeUnit.SECONDS).build<Message, Message>()
    val messageRepository: PersistentMessageRepository = PersistentMessageRepository.getInstance(talksConfig.dao)

    companion object {
        val logger = LogManager.getLogger(TelegramSender::class.java)
        var instance: TelegramSender? = null

        @Synchronized
        fun getInstance(talksConfig: TalksConfiguration): TelegramSender {
            if (instance == null) {
                instance = TelegramSender(talksConfig)
            }
            return instance!!
        }
    }

    suspend fun start() {
        if (started.get()) {
            return
        }

        startMutex.withLock {
            if (! started.get()) {
                processor = Coroutines.launchDefault(Coroutines.Group.TELEGRAM_MESSAGE_SENDER) {
                    doStart()
                }
                started.set(true)
            }
        }
    }

    private suspend fun doStart() = coroutineScope {
        while (isActive) {
            val sent = sendFailedMessage()
            if (! sent) {
                val message = rendezvousQueue.getOneMessage()
                sendMessage(message)
            }
        }
    }

    private suspend fun sendMessage(message: Message) {
        val chatId = message.response.chatId
        TelegramSenderPerChat.get(chatId, talksConfig, this).sendMessage(message)
    }

    suspend fun enqueueFailedMessage(message: Message) {
        failedMessageQueue.send(message)
    }

    private suspend fun sendFailedMessage(): Boolean {
        val message = failedMessageQueue.tryReceive().getOrNull()
        return if (message != null) {
            sendMessage(message)
            true
        } else {
            false
        }
    }

    fun markMessageAsSent(message: Message) {
        messagesSentInLastSecondCache.put(message, message)
        val persistentMessage = messageRepository.readById(message.sendQueueDbId)
        if (persistentMessage != null) {
            messageRepository.delete(persistentMessage)
        }
    }

    fun sendCapacityFull(): Boolean {
        return messagesSentInLastSecondCache.size() >= getAllChatsMaxMessagesPerSecond()
    }

    fun shutdown() {
        failedMessageQueue.close()
        processor?.cancel()
    }

    private fun getAllChatsMaxMessagesPerSecond(): Int {
        return LibSetting.getInteger(LibSetting.ALL_CHATS_MAX_MESSAGES_PER_SECOND) - 1
    }
}
