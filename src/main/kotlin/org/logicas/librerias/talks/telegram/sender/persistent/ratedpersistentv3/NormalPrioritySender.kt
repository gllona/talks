package org.logicas.librerias.talks.telegram.sender.persistent.ratedpersistentv3

import org.logicas.librerias.talks.api.TalksConfiguration
import org.logicas.librerias.talks.engine.TalkResponse

class NormalPrioritySender(
    val talksConfig: TalksConfiguration
) {
    val rendezvousQueue = RendezvousQueue.getInstance(talksConfig)

    companion object {
        var instance: NormalPrioritySender? = null

        @Synchronized
        fun getInstance(talksConfig: TalksConfiguration): NormalPrioritySender {
            if (instance == null) {
                instance = NormalPrioritySender(talksConfig)
            }
            return instance!!
        }
    }

    suspend fun send(response: TalkResponse) {
        rendezvousQueue.acceptNormalPriority(response)
    }

    fun shutdown() {
        // nothing to do
    }
}
