package org.logicas.librerias.talks.telegram.sender.persistent.ratedpersistentv3

import com.pengrad.telegrambot.request.BaseRequest
import com.pengrad.telegrambot.response.BaseResponse
import com.pengrad.telegrambot.response.SendResponse
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import org.apache.logging.log4j.LogManager
import org.logicas.librerias.talks.Coroutines
import org.logicas.librerias.talks.api.TalksConfiguration
import org.logicas.librerias.talks.config.LibSetting
import org.logicas.librerias.talks.engine.MediaType
import java.io.FileNotFoundException
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.math.max

class TelegramSenderPerChat(
    val chatId: Long,
    val talksConfig: TalksConfiguration,
    val telegramSender: TelegramSender
) {
    var processor: Job? = null
    val startMutex = Mutex()
    val started = AtomicBoolean(false)
    val sendQueue = Channel<Message>(Channel.UNLIMITED)
    val tracer = talksConfig.tracer

    companion object {
        var senders = ConcurrentHashMap<Long, TelegramSenderPerChat>()
        val logger = LogManager.getLogger(TelegramSenderPerChat::class.java)

        fun get(
            chatId: Long,
            talksConfig: TalksConfiguration,
            telegramSender: TelegramSender
        ): TelegramSenderPerChat {
            return senders.getOrPut(chatId) { TelegramSenderPerChat(chatId, talksConfig, telegramSender) }
        }

        fun shutdownAll() {
            senders.values.forEach { it.shutdown() }
        }
    }

    suspend fun start() {
        if (started.get()) {
            return
        }

        startMutex.withLock {
            if (! started.get()) {
                processor = Coroutines.launchIO(Coroutines.Group.TELEGRAM_MESSAGE_SENDER) {
                    doStart()
                }
                started.set(true)
            }
        }
    }

    private suspend fun doStart() = coroutineScope {
        var lastSendTimestamp = System.currentTimeMillis() - getInterMessageDelay()

        while (isActive) {
            delayForThisChat(lastSendTimestamp)
            if (isActive) {
                val message = sendQueue.receive()
                delayForAllChats()
                lastSendTimestamp = System.currentTimeMillis()
                doSendMessage(message)
            }
        }
    }

    private suspend fun delayForThisChat(lastSendTimestamp: Long) {
        val now = System.currentTimeMillis()
        val millisSinceLastSend = now - lastSendTimestamp
        val toDelay = max(0, getInterMessageDelay() - millisSinceLastSend)
        if (toDelay > 0) {
            delay((getInterMessageDelay() * 1.01).toLong()) // add 1% to play safe
        }
    }

    private suspend fun delayForAllChats() = coroutineScope {
        while (isActive && telegramSender.sendCapacityFull()) {
            delay(getInterMessageDelay() / 5) // 200 ms
        }
    }

    suspend fun sendMessage(message: Message) {
        start()
        sendQueue.send(message)
    }

    private suspend fun doSendMessage(message: Message) = coroutineScope {
        Coroutines.launchIO(Coroutines.Group.TELEGRAM_MESSAGE_SENDER, ephemeral = true) {
            try {
                val telegramRequest = buildTelegramRequest(message)
                if (telegramRequest == null) {
                    tracer.error(
                        message.response.chatId,
                        "Exception evaluating building a Telegram request"
                    )
                } else {
                    val telegramResponse = talksConfig.telegramBot.execute(telegramRequest)
                    handle(message, telegramResponse)
                }
                telegramSender.markMessageAsSent(message)
            } catch (e: Exception) {
                when (e.cause) {
                    is FileNotFoundException -> {
                        telegramSender.markMessageAsSent(message) // do not retry the send operation
                        logger.error("Exception trying sending message to Telegram because file was not found.", e)
                    }
                    else -> {
                        telegramSender.enqueueFailedMessage(message)
                        logger.error("Exception trying sending message to Telegram, will retry...", e)
                    }
                }
            }
        }
    }

    private fun buildTelegramRequest(message: Message): BaseRequest<*, *>? {
        return when (message.response.mediaType) {
            MediaType.ACTION_DELETE_MESSAGE -> message.response.telegramDeleteMessageRequest
            else -> message.response.telegramSendRequest
        }
    }

    private fun handle(message: Message, telegramResponse: BaseResponse?) {
        when (telegramResponse) {
            is SendResponse -> {
                val telegramMessageId = telegramResponse.message().messageId()
                talksConfig.interactionsManager.handle(message.response, telegramMessageId?.toLong())
            }
            else -> {}
        }
    }

    fun shutdown() {
        sendQueue.close()
        processor?.cancel()
    }

    private fun getInterMessageDelay(): Long {
        return LibSetting.getInteger(LibSetting.CHAT_INTER_MESSAGE_DELAY_IN_MILLIS).toLong()
    }
}
