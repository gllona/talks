package org.logicas.librerias.talks.telegram.sender.persistent.ratedpersistentv3

import kotlinx.coroutines.Job
import org.apache.logging.log4j.LogManager
import org.logicas.librerias.talks.Coroutines
import org.logicas.librerias.talks.api.TalksConfiguration
import org.logicas.librerias.talks.engine.SendPriority
import org.logicas.librerias.talks.engine.TalkResponse
import org.logicas.librerias.talks.telegram.MessageSenderApi

// Telegram limits:
// * 4096- chars/msg (DONE at superclass)
// * 30 msgs/sec to multiple chats (DONE)
// * 1 msgs/sec same chat (DONE)
// * 20 msgs/min same group (not planned yet)

class RatedPersistentMessageSenderV3Facade(
    val talksConfig: TalksConfiguration
) : MessageSenderApi() {

    var initProcessor: Job? = null
    val rendezvousQueue = RendezvousQueue.getInstance(talksConfig)
    val telegramSender = TelegramSender.getInstance(talksConfig)
    val normalPrioritySender = NormalPrioritySender.getInstance(talksConfig)
    val lowPrioritySender = LowPrioritySender.getInstance(talksConfig)

    companion object {
        const val DEEP_LOG = false
        private val logger = LogManager.getLogger(RatedPersistentMessageSenderV3Facade::class.java)
    }

    init {
        initProcessor = Coroutines.forceLaunchDefault(Coroutines.Group.MESSAGE_SENDER) {
            startSender()
        }

        logger.info("RatedPersistentMessageSenderV3 started.")
    }

    private suspend fun startSender() {
        telegramSender.start()
    }

    override fun send(response: TalkResponse) {
        Coroutines.forceLaunchDefault(Coroutines.Group.MESSAGE_SENDER, ephemeral = true) {
            when (response.priority!!) {
                SendPriority.NORMAL -> normalPrioritySender.send(response)
                SendPriority.LOW -> lowPrioritySender.send(response)
            }
        }
    }

    override fun shutdown() {
        logger.info("Stopping rated persistent message sender V3...")
        initProcessor?.cancel()
        normalPrioritySender.shutdown()
        lowPrioritySender.shutdown()
        telegramSender.shutdown()
        TelegramSenderPerChat.shutdownAll()
        rendezvousQueue.shutdown()
        logger.info("Message sender stopped.")
    }
}
