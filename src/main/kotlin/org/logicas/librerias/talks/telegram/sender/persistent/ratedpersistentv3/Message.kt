package org.logicas.librerias.talks.telegram.sender.persistent.ratedpersistentv3

import org.logicas.librerias.talks.engine.TalkResponse

class Message(
    val response: TalkResponse,
    val sendQueueDbId: Long
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Message

        if (sendQueueDbId != other.sendQueueDbId) return false

        return true
    }

    override fun hashCode(): Int {
        return sendQueueDbId.hashCode()
    }
}
