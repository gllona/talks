package org.logicas.librerias.talks.telegram

import com.pengrad.telegrambot.TelegramBot
import com.pengrad.telegrambot.TelegramException
import com.pengrad.telegrambot.UpdatesListener
import com.pengrad.telegrambot.model.Message
import com.pengrad.telegrambot.model.Update
import com.pengrad.telegrambot.request.GetFile
import com.pengrad.telegrambot.request.GetUpdates
import org.apache.logging.log4j.LogManager
import org.logicas.librerias.talks.Coroutines
import org.logicas.librerias.talks.api.InputAdapterApi
import org.logicas.librerias.talks.api.ModelApi
import org.logicas.librerias.talks.api.TalksConfiguration
import org.logicas.librerias.talks.api.TracerApi
import org.logicas.librerias.talks.config.LibSetting
import org.logicas.librerias.talks.engine.MediaType
import org.logicas.librerias.talks.engine.TalkResponse
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.net.URL
import java.nio.channels.Channels
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*
import java.util.concurrent.CountDownLatch

/**
 * based on work by stas on 4/5/16.
 */
class TelegramInputAdapter(
    private val talksConfig: TalksConfiguration
) : BotHandler(), InputAdapterApi {

    companion object {
        private val logger = LogManager.getLogger(TelegramInputAdapter::class.java)
    }

    private val token: String = getTelegramApiKey()
    private val bot: TelegramBot = TelegramBot.Builder(token).build() // TelegramBot.Builder(token).debug().build();
    private var model: ModelApi? = null
    private val tracer: TracerApi = talksConfig.tracer

    init {
        talksConfig.telegramBot = bot
        talksConfig.messageSender // start sender
        talksConfig.interactionsManager // start interactions manager
    }

    override fun getToken(): String {
        return token
    }

    override fun getBot(): TelegramBot {
        return bot
    }

    override fun onWebhookUpdate(update: Update) {
        assertModelIsSet()
        val updateId = update.updateId()
        try {
            tracer.info(
                getChatId(update), String.format("Entered onWebhookUpdate for Update [%d] with text [%s]",
                    updateId,
                    Optional.ofNullable(getMessage(update))
                        .map { obj: Message -> obj.text() }
                        .orElse(null)
                ))
            val response = model!!.receiveMessage(
                this,
                update
            )
            if (response.isPresent) {
                sendMessage(response.get(), updateId)
            }
        } catch (e: Exception) {
            tracer.error(getChatId(update), String.format("Exception when handling Update [%d]", updateId), e)
        } finally {
            tracer.info(getChatId(update), String.format("To exit onWebhookUpdate for Update [%d]", updateId))
        }
    }

    private fun sendMessage(
        response: TalkResponse,
        updateId: Int?
    ) {
        var nextResponse : TalkResponse? = response
        do {
            try {
                talksConfig.messageSender.send(nextResponse)
                talksConfig.talkManager.flushTalkContexts(response.chatId)

            } catch (e: Exception) {
                tracer.error(updateId?.toLong(), String.format("Exception trying to generate response for Update [%d", updateId), e)
                val exceptionalResponse = nextResponse?.exceptionMapper?.apply(e)
                if (exceptionalResponse != null) {
                    try {
                        talksConfig.messageSender.send(exceptionalResponse)
                    } catch (e2: Exception) {
                        tracer.error(updateId?.toLong(), String.format("Exception trying to generate exceptional response for Update [%d", updateId), e2)
                    }
                }
            }
            tracer.info(
                getChatId(nextResponse!!), String.format(
                    "Answered to Update [%s] with [%s]",
                    updateId,
                    if (nextResponse.mediaType != MediaType.TEXT) nextResponse.mediaType
                    else nextResponse.fastText?.replace("\n", "\\n")
                )
            )
            nextResponse = nextResponse.nextResponse
        } while (nextResponse != null)
    }

    private fun getChatId(response: TalkResponse): Long {
        return response.chatId
    }

    private fun getChatId(update: Update): Long? {
        val message = getMessage(update)
        return if (message?.chat() != null) { message.chat().id() }
        else null
    }

    private fun getMessage(update: Update): Message? {
        if (update.message() != null) {
            return update.message()
        } else if (update.editedMessage() != null) {
            return update.editedMessage()
        } else if (update.channelPost() != null) {
            return update.channelPost()
        } else if (update.editedChannelPost() != null) {
            return update.editedChannelPost()
        }
        return null
    }

    private fun assertModelIsSet() {
        if (model == null) {
            throw RuntimeException("Telegram Adapter: Model not set")
        }
    }

    // suspend not needed but stays here for future implementation not based on Coroutines.forceLaunchDefault
    override suspend fun dispatch(model: ModelApi) {
        this.model = model
        setupBot()
    }

    @Suppress("BlockingMethodInNonBlockingContext")
    private fun setupBot() {
        bot.setUpdatesListener(
            ::updatesListener,
            ::exceptionHandler,
            buildGetUpdatesRequest()
        )
    }

    private fun exceptionHandler(telegramException: TelegramException) {
        tracer.error(-1, "Exception in Telegram updates listener", telegramException)
    }

    private fun buildGetUpdatesRequest(): GetUpdates {
        val getUpdates = GetUpdates()
        getUpdates.limit(getTelegramGetUpdatesLimit())
        return getUpdates
    }

    private fun updatesListener(updates: List<Update>): Int {
        model!!.beforeReceiveMessages(this, updates)

        val latch = CountDownLatch(updates.size)
        updates.forEach {
            Coroutines.forceLaunchDefault(Coroutines.Group.TELEGRAM_INPUT_ADAPTER, ephemeral = true) {
                onWebhookUpdate(it)
                latch.countDown()
            }
        }
        latch.await()

        model!!.afterReceiveMessages(this, updates)

        return UpdatesListener.CONFIRMED_UPDATES_ALL
    }

    override fun sendResponse(response: TalkResponse) {
        sendMessage(response, null)
    }

    override fun getFile(fileId: String): File {
        return try {
            // telegram
            val request = GetFile(fileId)
            val response = bot.execute(request)
            val link = bot.getFullFilePath(response.file())
            // filesystem
            val basePath = getFileDownloadPath()
            Files.createDirectories(Paths.get(basePath))
            val targetFileName = basePath + "/" + response.file().fileUniqueId()
            // download
            download(link, targetFileName)
        } catch (e: IOException) {
            logger.error(String.format("Unable to create directory [%s] or download file", getFileDownloadPath()))
            throw e
        }
    }

    private fun download(url: String, filename: String): File {
        return try {
            val `in` = URL(url).openStream()
            val readableByteChannel = Channels.newChannel(`in`)
            val fileOutputStream = FileOutputStream(filename)
            val fileChannel = fileOutputStream.channel
            fileChannel.transferFrom(readableByteChannel, 0, Long.MAX_VALUE)
            File(filename)
        } catch (e: IOException) {
            logger.error(String.format("Error downloading [%s] to [%s]", url, filename), e)
            throw e
        }
    }

    override suspend fun shutdown() {
        bot.removeGetUpdatesListener()
        talksConfig.interactionsManager.shutdown()
        Coroutines.shutdown(Coroutines.Group.TELEGRAM_INPUT_ADAPTER)
        Coroutines.shutdown(Coroutines.Group.INTERACTIONS_MANAGER)
        Coroutines.shutdown(Coroutines.Group.MESSAGE_SENDER)
        Coroutines.shutdown(Coroutines.Group.TELEGRAM_MESSAGE_SENDER)
        talksConfig.messageSender.shutdown()
    }

    private fun getFileDownloadPath(): String {
        return LibSetting.get(LibSetting.FILE_DOWNLOAD_PATH)
    }

    private fun getTelegramApiKey(): String {
        return LibSetting.get(LibSetting.TELEGRAM_API_KEY)
    }

    private fun getTelegramGetUpdatesLimit(): Int {
        return LibSetting.getInteger(LibSetting.TELEGRAM_GET_UPDATES_LIMIT)
    }
}
