package org.logicas.librerias.talks.engine

import org.logicas.librerias.talks.api.InputAdapterApi
import org.logicas.librerias.talks.api.InputAdaptersManagerApi
import org.logicas.librerias.talks.api.TalksConfiguration
import org.logicas.librerias.talks.config.InputAdapters

class InputAdaptersManager(
    private val talksConfig: TalksConfiguration
) : InputAdaptersManagerApi {

    companion object {
        private var instance: InputAdaptersManager? = null

        @JvmStatic
        @Synchronized
        fun getInstance(talksConfig: TalksConfiguration): InputAdaptersManager {
            if (instance == null) {
                instance = InputAdaptersManager(talksConfig)
            }
            return instance as InputAdaptersManager
        }
    }

    private val inputAdapters: MutableList<InputAdapterApi> = ArrayList()

    override suspend fun startInputAdapters() {
        try {
            for (adapterType in InputAdapters.values()) {
                val adapter = adapterType.adapter.getDeclaredConstructor(TalksConfiguration::class.java).newInstance(talksConfig)
                adapter.dispatch(talksConfig.model)
                inputAdapters.add(adapter)
            }
        } catch (e: Exception) {
            throw RuntimeException("Exception starting InputAdapters", e)
        }
    }

    override suspend fun stopInputAdapters() {
        for (adapter in inputAdapters) {
            adapter.shutdown()
        }
    }
}
