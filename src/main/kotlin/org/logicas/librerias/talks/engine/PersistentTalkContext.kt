package org.logicas.librerias.talks.engine

import kotlinx.coroutines.delay
import org.apache.logging.log4j.LogManager
import org.logicas.librerias.copersistance.Dao
import org.logicas.librerias.talks.Coroutines
import org.logicas.librerias.talks.Coroutines.forceLaunchIO
import org.logicas.librerias.talks.config.LibSetting
import java.util.concurrent.ConcurrentHashMap

class PersistentTalkContext(dao: Dao, private val talk: Talk) : TalkContext() {

    companion object {
        private val logger = LogManager.getLogger(
            PersistentTalkContext::class.java
        )
        private const val GLOBAL_SESSION_ID: Long = -1
    }

    private data class CacheKey(
        val session: Long = 0,
        var type: String? = null
    )

    private val globalHandlersContext = InMemoryTalkContext()
    private val personalDataRemover: PersonalDataRemover
    private val repository: PersistentTalkContextRepository
    private val cache: MutableMap<CacheKey, PersistentTalkContextEntry> = ConcurrentHashMap()

    init {
        personalDataRemover = PersonalDataRemover.getInstance(dao)
        repository = PersistentTalkContextRepository.getInstance(dao)
    }

    override fun getGlobalHandlers(): List<InputHandler> {
        return globalHandlersContext.globalHandlers
    }

    override fun getBoundedHandlers(session: Long): List<InputHandler> {
        return repository.readHandlers(talk.getType(), session)
    }

    override fun deleteBoundedHandlers(session: Long) {
        repository.deleteHandlers(session)
    }

    @Synchronized
    override fun setGlobalHandlers(handlers: List<InputHandler>) {
        globalHandlersContext.globalHandlers = handlers
    }

    override fun setHandlers(session: Long, handlers: List<InputHandler>?) {
        if (handlers == null || Talk.preserveHandlers() == handlers) {
            return
        }
        var entry = cached(session, PersistentTalkContextEntry.TYPE_INPUT_HANDLERS)
        if (entry == null) {
            entry = repository.readHandlersEntry(talk.getType(), session)
            if (entry == null) {
                entry = PersistentTalkContextEntry(
                    session,
                    talk.getType().name,
                    PersistentTalkContextEntry.TYPE_INPUT_HANDLERS
                )
            }
            cache(entry)
        }
        entry.setInputHandlers(handlers)
    }

    @Synchronized
    override fun setGlobal(name: String, value: Any?) {
        set(GLOBAL_SESSION_ID, name, value)
    }

    override fun getGlobal(name: String): Any? {
        return get(GLOBAL_SESSION_ID, name)
    }

    override fun set(session: Long, name: String, value: Any?) {
        var entry = cached(session, PersistentTalkContextEntry.TYPE_VARS)
        if (entry == null) {
            entry = repository.readVarsEntry(talk.getType(), session)
            if (entry == null) {
                entry = PersistentTalkContextEntry(
                    session,
                    talk.getType().name,
                    PersistentTalkContextEntry.TYPE_VARS
                )
            }
            cache(entry)
        }
        val vars = entry.vars
        val finalValue = if (value is String) {
            TextEncoder.normalizeAndEncode(value)
        } else {
            value
        }
        vars[name] = finalValue
        entry.vars = vars
    }

    override fun remove(session: Long, name: String) {
        var entry = cached(session, PersistentTalkContextEntry.TYPE_VARS)
        if (entry == null) {
            entry = repository.readVarsEntry(talk.getType(), session)
            if (entry == null) {
                return
            }
            cache(entry)
        }
        val vars = entry.vars
        vars.remove(name)
        entry.vars = vars
    }

    override fun get(session: Long, name: String): Any? {
        var entry = cached(session, PersistentTalkContextEntry.TYPE_VARS)
        if (entry == null) {
            entry = repository.readVarsEntry(talk.getType(), session)
            if (entry == null) {
                return null
            }
            cache(entry)
        }
        var value = entry.vars[name]
        if (value is String) {
            value = TextEncoder.unencode(value as String?)
        }
        return value
    }

    override fun getAll(session: Long): Map<String, Any?> {
        var entry = cached(session, PersistentTalkContextEntry.TYPE_VARS)
        if (entry == null) {
            entry = repository.readVarsEntry(talk.getType(), session)
            if (entry == null) {
                return HashMap()
            }
            cache(entry)
        }
        return entry.vars
    }

    override fun reset(session: Long) {
        repository.deleteVars(talk.getType(), session)
        val varsCacheKey = CacheKey(session, PersistentTalkContextEntry.TYPE_VARS)
        cache.remove(varsCacheKey)
        repository.deleteHandlers(talk.getType(), session)
        val handlersCacheKey = CacheKey(session, PersistentTalkContextEntry.TYPE_INPUT_HANDLERS)
        cache.remove(handlersCacheKey)
    }

    override fun flush(session: Long) {
        flush(session, PersistentTalkContextEntry.TYPE_INPUT_HANDLERS)
        flush(session, PersistentTalkContextEntry.TYPE_VARS)
    }

    private fun flush(session: Long, entryType: String) {
        val cacheKey = CacheKey(session, entryType)
        try {
            val entry = cached(session, entryType)
            if (entry != null) {
                repository.save(entry)
                cache.remove(cacheKey)
            }
        } catch (e: Exception) {
            logger.warn(
                String.format(
                    "Unable to save PersistentTalkContext for session [%d] and type [%s] (cache flush)",
                    session,
                    entryType
                )
            )
            // don't throw the exception, this is mainly a concurrency issue between instances
        }
    }

    override fun forget(session: Long) {
        forceLaunchIO(Coroutines.Group.UTILS, ephemeral = true) {
            delay(LibSetting.getInteger(LibSetting.PERSONAL_DATA_REMOVAL_DELAY_IN_MILLIS).toLong())

            personalDataRemover.remove(session)
        }
    }

    private fun cache(entry: PersistentTalkContextEntry) {
        val cacheKey = CacheKey(entry.sessionId, entry.type)
        cache[cacheKey] = entry
    }

    private fun cached(session: Long, entryType: String): PersistentTalkContextEntry? {
        val cacheKey = CacheKey(session, entryType)
        return cache[cacheKey]
    }
}
