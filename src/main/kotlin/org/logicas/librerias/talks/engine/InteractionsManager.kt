package org.logicas.librerias.talks.engine

import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import org.apache.logging.log4j.LogManager
import org.logicas.librerias.talks.Coroutines
import org.logicas.librerias.talks.api.InteractionsManagerApi
import org.logicas.librerias.talks.api.TalksConfiguration
import org.logicas.librerias.talks.config.LibSetting
import org.logicas.librerias.talks.telegram.MessageSenderApi
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit

class InteractionsManager(
    talksConfig: TalksConfiguration
) : InteractionsManagerApi {

    companion object {
        private val logger = LogManager.getLogger(InteractionsManager::class.java)
        private var instance: InteractionsManager? = null

        @JvmStatic
        @Synchronized
        fun getInstance(talksConfig: TalksConfiguration): InteractionsManager {
            if (instance == null) {
                instance = InteractionsManager(talksConfig)
            }
            return instance as InteractionsManager
        }
    }

    private val messageSender: MessageSenderApi
    private val interactionRepository: InteractionRepository
    private var flyJob: Job? = null

    init {
        messageSender = talksConfig.messageSender
        interactionRepository = InteractionRepository.getInstance(talksConfig.dao)
        flyMessages()
        logger.info("InteractionsManager started.")
    }

    override fun handle(request: TalkRequest) {
        val flyTime = request.flyTime ?: return

        val messageId = request.message.messageId()
        if (messageId == null) {
            logger.error("InteractionsManager can not handle request with null inputAdapterId.")
            return
        }

        val now = LocalDateTime.now()

        val flyAtTime = now.plus(flyTime, ChronoUnit.MILLIS)

        val interaction = Interaction()
        interaction.type = Interaction.TYPE_REQUEST
        interaction.chatId = request.chatId
        interaction.createdAt = now
        interaction.messageId = messageId.toLong()
        interaction.flyAt = flyAtTime

        interactionRepository.save(interaction)
    }

    override fun handle(response: TalkResponse, messageId: Long?) {
        val flyTime = response.flyTime ?: return

        if (messageId == null) {
            logger.error("InteractionsManager can not handle response with null inputAdapterId.")
            return
        }

        val now = LocalDateTime.now()

        val flyAtTime = now.plus(flyTime, ChronoUnit.MILLIS)

        val interaction = Interaction()
        interaction.type = Interaction.TYPE_RESPONSE
        interaction.chatId = response.chatId
        interaction.messageId = messageId
        interaction.createdAt = now
        interaction.flyAt = flyAtTime

        interactionRepository.save(interaction)
    }

    private fun flyMessages() {
        flyJob = Coroutines.forceLaunchIO(Coroutines.Group.INTERACTIONS_MANAGER) {
            while (isActive) {
                delay(getFlyJobLoopDelayInMillis())
                if (isActive) {
                    val interactions = readInteractionsToFly()
                    interactions.forEach {
                        if (isActive) {
                            try {
                                fly(it)
                                deleteInteraction(it)
                            } catch (e: Exception) {
                                logger.error("Exception trying to make an interaction to fly.", e)
                            }
                        }
                    }
                }
            }
        }
    }

    private fun readInteractionsToFly(): List<Interaction> {
        val flyingMessagesStealthFactor = getFlyingMessagesStealthFactor()

        return if (flyingMessagesStealthFactor == 1)
            interactionRepository.readToFly()
            else interactionRepository.readToFlyStealth(flyingMessagesStealthFactor)
    }

    private fun fly(interaction: Interaction) {
        val flyAction = TalkResponse.ofFlying(
            interaction.chatId,
            interaction.messageId
        )

        messageSender.send(flyAction)
    }

    private fun deleteInteraction(interaction: Interaction) {
        interactionRepository.delete(interaction)
    }

    override fun getFlyingMessagesCount(): Int {
        return interactionRepository.countAll("fly_at IS NOT NULL")
    }

    override fun shutdown() {
        flyJob?.cancel()
        logger.info("InteractionsManager stopped.")
    }

    private fun getFlyingMessagesStealthFactor(): Int {
        return Math.max(1, LibSetting.getInteger(LibSetting.FLYING_MESSAGES_STEALTH_FACTOR))
    }

    private fun getFlyJobLoopDelayInMillis(): Long {
        return LibSetting.getInteger(LibSetting.CHAT_INTER_MESSAGE_DELAY_IN_MILLIS).toLong()
    }
}
