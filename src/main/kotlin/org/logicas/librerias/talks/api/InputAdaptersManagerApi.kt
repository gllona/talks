package org.logicas.librerias.talks.api

interface InputAdaptersManagerApi {

    suspend fun startInputAdapters()

    suspend fun stopInputAdapters()
}
