package org.logicas.librerias.talks.filesystem

import kotlinx.coroutines.delay
import org.logicas.librerias.talks.Coroutines
import org.logicas.librerias.talks.api.DeleteQueueApi
import java.io.File
import java.util.concurrent.TimeUnit

object DeleteQueue : DeleteQueueApi {

    override fun deleteAfter(file: File, duration: Int, timeUnit: TimeUnit) {
        Coroutines.forceLaunchIO(Coroutines.Group.UTILS, ephemeral = true) {
            delay(getDelayMillis(duration, timeUnit))
            if (file.exists()) {
                file.delete()
            }
        }
   }

    private fun getDelayMillis(duration: Int, timeUnit: TimeUnit): Long {
        return TimeUnit.MILLISECONDS.convert(duration.toLong(), timeUnit)
    }
}
