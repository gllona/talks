# Talks: Telegram chatbot mini-framework

You want a chatbot but need a framework. You need a framework because you need more than a client library, more than a HTTP request/response solution, to express conversational interactions with a complete system.

Then you arrive to the conclusion is that you need more than a chatbot maker, you need something more specific than what the many cloud-based chatbot services provide with extreme easyness. You also want to host your system.

You are familiar with Java, and know what is a framework. Then you can use Talks for your own Telegram chatbots.

# Advantages

* More than an HTTP-based library. Talks is a framework that provides routing, context management and security.
* You can develop your own bots with all the power of Java. You are not restricted to the limits of cloud-based bot generators.
* You can develop complete applications without the hassle of front-end development. Telegram manages it for you.

# Easy try

Clone [this example project](https://gitlab.com/gllona/talks-example) and give Talks a try. It comes with documentation and you will need less than 15 minutes for running it from scratch, honestly, if you already have the Java JDK and a MySql Server installed in your machine.  

Behind the scenes, all Telegram traffic is routed using Stas Parshin [Java Telegram Bot API](https://github.com/pengrad/java-telegram-bot-api).

# What Talks provides

As every framework, Talks provides specific functions that allows you to focus on the domain and not on the quirks of the user-to-app-to-user data transmission:

## Routing

In almost every REST application framework, you have a way to define what should be executed according to an HTTP phrase (HTTP method, URL, arguments). Some frameworks define the routes in specific files, while others rely on code annotations.

The main difference between REST applications and conversational ones is that with chatbots you have less information for routing. In most of the cases, you get no more than a single word. Consider the following scenario in which the *handler* represents a Java method:

```
#       USER INPUT      BOT HANDLER             BOT RESPONSE
--------------------------------------------------------------------------------------------
1.      /start          start()                 Do you want pizza?
2.      Yes             poorPizzaRestaurant()   We offer Napolitana only, do you want one?
3.      Yes             waitForYourPizza()      Great. Take a seat and wait a few minutes.     
``` 

The bot should recognize that a user input `Yes` means something different according to the interactional moment, so the correct handler is executed. This means that the architecture of the REST applications routing:

```
* HTTP phrase --> routing table --> Java handler
* Java handler execution --> HTTP response
``` 

is not applicable to chatbots, because there is no 1:1 relationship between user inputs and handlers. It's instead an 1:N relationship.

In order to provide a proper routing, Talks rely on the *context* (see below) for implementing the following architecture:

```
* [pre-condition] Context from interaction N-1 is stored at the backend
* (User input for interaction N) + (context) --> Java handler
* Java handler execution --> (bot response for interaction N) + (mutated context)
* [post-condition] Context from interaction N is stored at the backend
```  

## Context Management

The *context* is the *state* associated to a Telegram bot user that is stored at the backend.

In Talks, the *context* is composed by two elements:

* The current *routing table*
* The *session variables*

The *context* is associated with a specific Telegram user. Each Telegram user is identified to the bot with its `chat_id`.

A user can have a long-lived session with the bot (in some cases, years-long). Talks doesn't store the session history, only the information for handling inputs *at an specific moment - the current moment* of the user session.

The context is persistent. It's stored in a MySql table named `talk_contexts`. Each user *context* is represented by two rows in the table. 

### The Routing Table

One of the DB table's rows, with `type == 'handlers'`, stores the routing table. The routing table is dynamic -different than a REST application- and depends on the user session moment. The `content` column will contain something similar as:

```json
[
  {
    "triggers": [
      {
        "mediaType": "TEXT",
        "text": "Yes",
        "isRegex": false
      }
    ],
    "className": "WelcomeTalk",
    "methodName": "waitForYourPizza"
  }
]
```

This entry means that when a user input with the text `Yes` arrives, it will be routed to the `waitForYourPizza` method in the `WelcomeTalk` class.

The JSON above represents an array of *handlers*. There can be more than one handler for a specific moment of the user session. Each handler is associated with a class and method, and can be executed if one of its *triggers* matches with the user input. There can be more than one trigger for each handler. Also, triggers can be defined using regular expressions. And, of course, Talks supports the rich interaction provided by Telegram, meaning that you can define handlers that answer to *Photos, Audios, Videos,* etc.

### The Session Variables

As with every system, backend state can be stored in a RDBMS or cache. But in conversational applications there are lots of pieces of information that should be stored. This is because chatbots lack client-side state.

In a mobile app, the screen is made by assembling components that store the pieces of the user input. After all pieces have been fulfilled by the user, an HTTP request is made to the backend. But a Telegram chatbot can't handle them, because its memory is short-lived: a request, a response.

Suppose that you want to acquire some parts of the user input and then execute a domain-specific handler:

```
#       USER INPUT      BOT HANDLER                     BOT RESPONSE
--------------------------------------------------------------------------------------------
4.      Napolitana      offerNapolitanaToppings()       Do you want toppings?
                                                        * Anchovies
                                                        * Onions
                                                        * No
5.      Onions          offerNapolitanaToppings()       An additional topping?
                                                        * Anchovies
                                                        * No
6.      Anchovies       offerNapolitanaToppings()       An additional topping?
                                                        * No
7.      No              waitForYourPizza()              Great. Take a seat and wait a few minutes.     
``` 

In this scenario, you want to gather the pizza toppings before calling the `waitForYourPizza` method. You could implement a DB table for that, but imagine doing that for every set of multi-step domain actions. You end with a lot of intermediate tables.

The *session variables* offered by Talks allows you to collect that information before calling the domain method. You can store *strings*, *integer numbers* and *floating point numbers*. The *session variables* are modeled after a (name, value) map. 

In the `talk_contexts` table, this is represented by a row with `type == 'vars'`, which has a `content` column with a value as:

```json
{
  "PIZZA": "Napolitana",
  "TOPPINGS": "Onions,Anchovies"
}
```

Then, when the method `waitForYourPizza` is executed, it can recover the values of the *variables* `PIZZA` and `TOPPINGS`, and then proceed to the kitchen.

### User Privacy

The `TalkContext` object available in every `Talk` subclass provides a method `forgetPersonalData`
to remove the user's personal data stored in the `talk_contexts` table.

## Limits Management

Telegram imposes some limits to bots:

* 30 messages per second sent to all chats
* 1 second between consecutive messages sent to the same chat
* 4096 characters per text message

Talks manages those limits for you.

Note, however, that the text message length limit is not enforced for HTML responses, only for plain text messages. Sending a very long HTML message could fail silently.

## Security

The *dynamic routing table* implemented by Talks offer an entry-level security layer. Every domain handler -a Java method- can be executed only when it's in the user's *routing table* and the user input matches an associated *trigger*. In no other case a handler will be executed by Talks.

When a user starts a chatbot session, there are no handlers available for the user. But as every conversational segment -represented as a `Talk` subclass- has an optional *default handler*, you can define, let's say, a `WelcomeTalk` class which inherits from `Talk` and provides a default handler, a method `doStart()`, that will answer when the trigger `/start` matches the user input.

In this way, a *handler* will be executed only if:

* It has been defined as the method to be called after a specific user input matches the handler's *trigger*
* That definition has been set:
  * In the *default handler* definition of a `Talk` subclass, or
  * In the user session *routing table*, as a product of an explicit, code-level definition done when a previously executed *handler* returned its *response* to the user 
 
This differs from REST applications, where the backend endpoints are openly available at every moment of the user session, ready to be exploited using HTTP client automation.

## Easyness

This is the code for a `Talk` subclass that implements a `hello world` bot:

```java
public class WelcomeTalk extends Talk {

    public WelcomeTalk(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext();
    }

    @Override
    public Optional<InputHandler> getDefaultHandler() {
        return Optional.of(
            InputHandler.of("WelcomeTalk::doStart", InputTrigger.ofString("/start"))
        );
    }

    public TalkResponse doStart(TalkRequest request) {
        return TalkResponse.ofText(
            request.getChatId(),
            "hello world!"
        );
    }
}
```

If you want to implement a simple flow, you can attach *triggers* to the `TalkResponse` that is returned by `doStart()`, and implement the required *handlers*. Each *handler* comes with a very specific signature, they receive a `TalkRequest` and return a `TalkResponse`:

```java
    public TalkResponse doStart(TalkRequest request) {
        return TalkResponse.ofText(
            request.getChatId(),
            "hello world!\n" 
                + "To continue tap /here"
        ).withString("WelcomeTalk::continueAfterStart", "/here");
    }
    
    public TalkResponse continueAfterStart(TalkRequest request) {
        return TalkResponse.ofText(
            request.getChatId(),
            "you made a two step interaction!"
        );
    }
```

The Talks approach means that every *handler* has to be prepared to process only the specific user input type that is defined by the user session *triggers*. For example, if you define:

```
#       EXECUTED METHOD                 RESPONSE.HANDLER.TRIGGER    RESPONSE.HANDLER.METHOD      
--------------------------------------------------------------------------------------------
8.      offerNapolitanaToppings()       * regex: [A-Za-z]+          offerNapolitanaToppings() 
                                        * No                        waitForYourPizza()
```

then the `waitForYourPizza()` method should be prepared to receive a text input only, and in this particular case that text will be a `No`. No need to prepare that *handler* to cope with inputs of type *Photo*, *Audio*, *Video*, etc. Note, however, that you could define other *triggers* that will make the `waitForYourPizza()` to be executed as the product of other user input types.  

## Multiple Responses

If you want to send multiple responses to an incoming interaction, use the following syntax:

```java
public TalkResponse cookPizza(TalkRequest request) {
    String photo = request.getChatId() % 2 == 0 ? 
        "cat.jpg" : "dog.jpg";
    request.getInputAdapter().sendResponse(
        TalkResponse.ofPhoto(
            request.getChatId(),
            Utils.fileFrom(photo)
        )
    );
    return TalkResponse.ofText(
        request.getChatId(),
        "Your pizza is cooking. Enjoy our patron's picture."
    );
}
```

**Note**: messages sent using `request.getInputAdapter().sendResponse(....)` can not set *handlers* to be used in next interactions. They, however, can be used to set how messages are shown by Telegram. A message sent with `.withPreserveHandlers()` will make Telegram don't change existing displayed buttons or keyboard in a chat.

## Low priority messages

Sometimes you want to send broadcast notifications to all your bot users. A massive amount of notifications, in the form of Telegram messages, can block the normal flow of interactions with users currently using your application. This is because Telegram imposes a limit of 30 messages per second sent to all chats. If you want to send 1800 broadcast messages, you are delaying future users' interactions for 1 complete minute, during which the users will not receive feedback from your application.

The solution is to send the broadcast notifications as *low priority* messages, using the following syntax:

```java
public TalkResponse notifyFreePizza(TalkRequest request) {
    for (int i = 0; i < 1000; i++) {
        request.getInputAdapter().sendResponse(
            TalkResponse.ofText(
                getChatIdFromYourUsersDatabase(i),
                "Hey come for a free pizza tonight!"
            ).withPreserveHandlers()
             .withLowPriority()
        );
    }
    
    return TalkResponse.ofText(
        request.getChatId(),
        "Notifications sent."
    );
}
```

Submitted low priority messages are scheduled to be sent at 1 per second speed.

**Note 1**: This feature is available only when you have `USE_PERSISTENT_MESSAGE_SENDER=1` in the `config.properties` file. Any other case, messages tagged as low priority will be considered as normal priority by the message sender.

**Note 2**: This feature has not been tested in depth. Please use it carefully. You should set `PERSISTENT_MESSAGE_SENDER_VERSION` to `2` (threads) or `3` (coroutines). If you use `3` wit this feature, use also `USE_SINGLE_THREAD=1`.

**Note 3**: If `PERSISTENT_MESSAGE_SENDER_VERSION` is `2` or `3`, the low priority messages are not persisted. They are lost in the event of a system failure.

## Flying messages

Flying messages or "disappearing messages" if a feature for which you can make messages sent or received by the bot to disappear after
some specific time. The Telegram bot API allows bots to delete messages in the first 48 hours after they are generated
(by the bot or by bot users).

In order to make a received message "to fly", you need to use this syntax:

```java
public TalkResponse deleteReceivedMessage(TalkRequest request) {
    request.fly(); // will delete the received message after a predefined duration
    // or
    request.fly(5, TimeUnit.MINUTES); // make the message fly after 5 minutes
    // ...    
}
```

In order to make a message sent "to fly":

```java
public TalkResponse sendMessageWithAutoFly(TalkRequest request) {
    // create a TalkResponse that will fly after a predefined duration
    TalkResponse flyingMessage1 = TalkResponse.ofText(
        request.getChatId(),
        "Flying message 1"
    ).withFlying();
    
    // create a TalkResponse that will fly after 1 hour
    TalkResponse flyingMessage2 = TalkResponse.ofText(
        request.getChatId(),
        "Flying message 2"
    ).withFlying(1, TimeUnit.HOURS);
    
    return flyingMessage1; // or flyingMessage2
}
```

The effective duration will be capped to the value of the environment variable `FLYING_MESSAGES_MAX_DURATION_IN_MINUTES`. To be sure that Talks has enough time
to process the deletion queue, communicate with the Telegram servers, cope with intermittent network failures, and have some space for
bot re-starts, it is recommended to use a maximum duration of 47 hours (or less).

If a duration is not specified, the duration will be taken from the environment variable `FLYING_MESSAGES_DURATION_IN_MINUTES`.
If this value is `-1` then the flying messages feature will be ignored and messages won't fly.

There is an additional environment variable you must set, `FLYING_MESSAGES_STEALTH_FACTOR`. Normally its value should be `1`.
But for testing purposes, you may want to check if the messages scheduled for hours in advance fly. If you set this value to `60`, then
a message scheduled to fly in 1 minute will fly in 1 second, and a message scheduled to fly in 1 hour
will fly in 1 minute. There is no upper limit for this value.

**Note**: Flying messages are available only in the `kotlin` branch and if `PERSISTENT_MESSAGE_SENDER_VERSION` is `3`.

## Asynchronous mode

Sometimes generating the contents to be displayed is an expensive operation, like image generation or
text generation using AI models. Those operations avoid sending a fast answer to Telegram servers. It is better to
delay the content generation to the message sender, which is asynchronous regarding the main event loop.

You can specify a `textSupplier` and a `fileSupplier` in each `TalkResponse`. Pass lambda functions to them and
at the same time pass `null` in its factory methods for arguments of type `String` (for the text or HTML message) and
`File`.

If a supplier fails its execution, you can resort to the `exceptionMapper`, which provides a fallback response to
each Exception thrown at asynchronous response content building.

You can also specify a chain of responses to be sent. Use the `next` method to build the response chain.

The following code excerpt shows how to apply this feature:

```java
public TalkResponse showFunImage(TalkRequest request) {
    TalkResponse funDescription = TalkResponse.ofText(
        request.getChatId(),
        "That was fun!"
    );
    TalkResponse funImage = TalkResponse.ofPhoto(
        request.getChatId(),
        null   // pass null here instead of a File object
    ).withFileSupplier(() -> funProvider())
     .withExceptionMapper(e -> cantCreateImage(request, e))
     .withButton("fun.FunTalk::continue", "Continue")
     .next(funDescription);
    return funImage;
}

private File funProvider() {
    File funImage = plotter.plot();   // expensive operation that can throw Exception
    filesystem.deleteAfter(funImage, 14, TimeUnit.SECONDS);   // keep the filesystem clean
    return funImage;
}

private TalkResponse cantCreateImage(TalkRequest request, Exception e) {
    return TalkResponse.ofText(
        request.getChatId(),
        "System error. Unable to paint image."
    ).withButton("fun.FunTalk::menu", "🏕 Back");
}
```

**Note**: Asynchronous responses and response chaining are available only in the `kotlin` branch and if `PERSISTENT_MESSAGE_SENDER_VERSION` is `3`.

# HOW_TO

You can follow these steps or base your project on one of these projects:

* [animal-welfare-telegram-bot](https://gitlab.com/gllona/animal-welfare-telegram-bot/-/blob/master/app/build.gradle)
* [talks-example](https://gitlab.com/gllona/talks-example)

## Setup

### 1. Gradle

Create a Gradle project in your IDE.

Copy the `build.gradle` from [here](https://gitlab.com/gllona/animal-welfare-telegram-bot/-/blob/master/app/build.gradle). Take note that this a module Gradle configuration file. You could also need a `settings.gradle` similar to [this](https://gitlab.com/gllona/animal-welfare-telegram-bot/-/blob/master/settings.gradle) if you opt for a multi-module project. 

Edit `build.gradle`:

```groovy
application {
    mainClassName = 'MAIN_CLASS_FULL_QUALIFIED_NAME'
    executableDir = '.'
}
```

### 2. Main class

Create your main class. Copy it from [here](https://gitlab.com/gllona/animal-welfare-telegram-bot/-/blob/master/app/src/main/java/org/animal/App.java).

Remove the reference to `AppConfig` inside the constructor.

### 3. Model class

You can use the *model class* as an interceptor for Telegram requests and responses. Copy it from [here](https://gitlab.com/gllona/animal-welfare-telegram-bot/-/blob/master/app/src/main/java/org/animal/Model.java).

### 4. Talks enum

`Talk` subclasses are registered in the `Talks` enum. Copy it from [here](https://gitlab.com/gllona/animal-welfare-telegram-bot/-/blob/master/app/src/main/java/org/animal/Talks.java) and remove all elements except `WelcomeTalk`.

### 5. Database

Create a MySql database and table `talk_contexts`:

```
CREATE TABLE talk_contexts (
    id INT NOT NULL AUTO_INCREMENT,
    session_id BIGINT NOT NULL,
    talk_name VARCHAR(255) NOT NULL,
    type VARCHAR(255) NOT NULL,
    content TEXT NOT NULL,
    PRIMARY KEY(id),
    UNIQUE KEY(session_id, talk_name, type)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1;
```

The "flying messages" feature requires the `talks_interactions` table:

```
CREATE TABLE talks_interactions(
    id BIGINT NOT NULL AUTO_INCREMENT,
    type CHAR(8) NOT NULL,
    chat_id BIGINT NOT NULL,
    message_id BIGINT NOT NULL,
    created_at TIMESTAMP NULL,
    fly_at TIMESTAMP NULL,
    PRIMARY KEY(id),
    INDEX interactions_fly_at_idx (fly_at)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1;
```

If you want to use the configuration flag `USE_PERSISTENT_MESSAGE_SENDER=1`, which will store the pending Telegram messages to 
send in a table in order to don't lose messages if the JVM process gets stopped, you will need also the following table:

```
CREATE TABLE telegram_message_sender_queue (
    id BIGINT NOT NULL AUTO_INCREMENT,
    talk_response TEXT NOT NULL,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1;
```

## Interactions

Create a `Talk` subclass with some interactions in it. The name will be `WelcomeTalk` as it was already registered in the `Talks` enum:

```java
public class WelcomeTalk extends Talk {

    public WelcomeTalk(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext();
    }

    @Override
    public Optional<InputHandler> getDefaultHandler() {
        return Optional.of(
            InputHandler.of("WelcomeTalk::start", InputTrigger.ofString("/start"))
        );
    }

    public TalkResponse start(TalkRequest request) {
        return TalkResponse.ofText(
            request.getChatId(),
            "Like?"
        ).withButton("WelcomeTalk::yes", "Yes")
         .withButton("WelcomeTalk::no", "No");
    }
    
    public TalkResponse yes(TalkRequest request) {
        return thanks(request);
    }

    public TalkResponse no(TalkRequest request) {
        return TalkResponse.ofText(
            request.getChatId(),
            "I will delete your vote."
        ).withButton("WelcomeTalk::thanks", "Continue");
    }

    private TalkResponse thanks(TalkRequest request) {
        return TalkResponse.ofText(
            request.getChatId(),
            "Thanks!\n" 
                + "Vote /again"
        ).withString("WelcomeTalk::start", "/again");
    }
}
```

## Configuration

Create a `config.properties` file. Copy it from [here](https://gitlab.com/gllona/talks-example/-/blob/master/config.properties.EXAMPLE).

Create an `.env` file. Copy it from [here](https://gitlab.com/gllona/talks-example/-/blob/master/.env.EXAMPLE)

Edit both files following the indications provided in the example project [Readme](https://gitlab.com/gllona/talks-example/-/tree/master/#configure-the-bot) for database settings and Telegram bot settings.

Note also that `TALK_PACKAGE_NAME` should tell the package name where your `WelcomeTalk` class (or other Talk subclasses) are placed. 
If this value is `org.app`...

* ... And you generate a `TalkResponse` with `.withString("Menu::menu", "/menu")`, then Talks
  will make the bot to execute the method `menu` in the `org.app.Menu` class if the user enters the `/menu` command
* ... And you generate a `TalkResponse` with `.withString("intro.Menu::menu", "/menu")`, then Talks
  will make the bot to execute the method `menu` in the `org.app.intro.Menu` class if the user enters the `/menu` command

## Run

In local environment:

```
./gradlew :run
```

Or run the App class in your favorite IDE.

In production environment:

```
./gradlew clean shadowJar
java -jar build/libs/app-all.jar
```

Note that your generated fat JAR could have a different name.

# Disclaimer

Code is not unit tested, yet. Use it at your own risk.

# Author

Idea and development by [Gorka Llona](http://desarrolladores.logicos.org/gorka/).
